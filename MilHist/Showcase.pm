package MilHist::Showcase;

# MilHist::Showcase.pm - handle MilHist showcase pages 
#    19 Jun 18 Capitalise Unicode::Collate
#    15 Feb 2019 Unescaped left brace is now deprecated

use English;
use strict;
use utf8;
use warnings;

use Carp;
use Unicode::Collate;

my %fields;

sub new {
	my $that = shift;
	my $class = ref ($that) || $that;
	my $self = { %fields };
	bless $self, $class;

	$self->{text} = shift;
	$self->{collator} = new Unicode::Collate ();

	return $self;
}

sub article ($$) {
	my ($self, $line) = @ARG;
	foreach ($line) {
		my $article;
		if (/\[\[(.+?)[\]\|]/) {
			$article = $1;
		} elsif (/\{\{(HMS|HMAS|ship|SMS|SS|MS|USS)\|.+?\}\}/) {
			my $ship;
			if (/\{\{SMS\|(.+?)\|(.+?)\|sub=y\}\}/) {
				$ship = "SM $1 ($2)";
			} elsif (/\{\{SMS\|(.+?)\|sub=y\}\}/) {
				$ship = "SM $1";
			} elsif (/\{\{(HMS|HMAS|SMS|SS|MS|USS)\|(.+?)\|(.+?)\}\}/) {
				$ship = "$1 $2 ($3)";
			} elsif (/\{\{(HMS|HMAS|SMS|SS|MS|USS)\|(.+?)\}\}/) {
				$ship = "$1 $2";
			} elsif (/(ship)\|(.+)\|(.+)\}\}/) {
				$ship = "$2 $3";
			} elsif (/(ship|SMS|MS|SS)\|(.+)\}\}/) {
				$ship = "$1 $2";
			} else {
				warn "ERROR: ", $ARG, "\n";
			}	
			$article = $ship;
		} else {
			warn "ERROR: ", $ARG, "\n";
		}
		return $article;
	}
}

sub add ($$) {
	my ($self, $page) = @ARG;
		
	my @lines = split (/^/, $self->text);
	my @output;
	my $inserted = 0;
	
	foreach my $line (@lines) {
		if (!$inserted) {
			my $do_insert = 0;
			
			if ($line =~ /^\*/) {
				my $article = $self->article ($line);
				my $result = $self->collator->cmp ($article, $page);
				if ($result > 0) {
					push @output, "* [[$page]]\n";
					$inserted = 1;
				}
			} elsif ($line =~ /^; (.)$/ ) {
				my $heading = $1;
#				print $1, "\n";
				my $result = $self->collator->cmp ($heading, substr ($page, 0, 1));
				if ($result > 0) {
					$do_insert = 1;
				}
			} elsif ($line =~ /^\{\{col-end\}\}/ ) {
				$do_insert = 1;
			}
			
			if ($do_insert) {
				my @stack;
				while (1) {
					my $temp = pop @output;
					push @stack, $temp;
					last if $temp =~  /^\*|^;/;
				}
				push @output, pop @stack;
				push @output, "* [[$page]]\n";
				push @output, reverse @stack;
				$inserted = 1;
			}
		}
		push @output, $line;
	}	
		
	my $text = join '', @output;
	$text =~ /<!--COUNT-->(\d+)<!--COUNT-->/;
	my $count = $1 + 1;
	$text =~ s/<!--COUNT-->\d+<!--COUNT-->/<!--COUNT-->$count<!--COUNT-->/s;
	$self->{text} = $text;
	return 1;
}

sub del ($$) {
	my ($self, $page) = @ARG;
	
	my @lines = split (/^/, $self->text);
	my @output;
	my $deleted = 0;
	
	foreach my $line (@lines) {
		unless ($deleted) {
			if ($line =~ /^\*/) {
				my $article = $self->article ($line);
				my $result = $self->collator->cmp ($article, $page);
				if (0 == $result) {
					$deleted = 1;
					next;
				}
			}
		}
		push @output, $line;
	}	

	return 0 unless $deleted;
	
	my $text = join '', @output;
	$text =~ /<!--COUNT-->(\d+)<!--COUNT-->/;
	my $count = $1 - 1;
	$text =~ s/<!--COUNT-->\d+<!--COUNT-->/<!--COUNT-->$count<!--COUNT-->/s;
	$self->{text} = $text;
	return 1;
}

sub check ($) {
	my ($self) = @ARG;
	
	my $last = '';
	my $count = 0;
	my @line = split /\n/, $self->text;
	foreach (@line) {
		if (/^\*/) {
			my $article = $self->article ($ARG);
#			print "'$article'\n";
			++$count;
			
			if ($self->collator->cmp ($last, $article) > 0) {
				warn "Our of order: $article\n";
			}
			$last = $article;		
		}
	}
	return $count;
}

sub collator ($) {
	my ($self) = @ARG;
	return $self->{collator};
}

sub text ($) {
	my ($self) = @ARG;
	return $self->{text};
}

1;