#!/usr/bin/perl -w
#
# fanm.pl -- Create the WP:FANMP page from the FA page
# Usage: fanmp.pl
#
# 21 Sep 2014 Created
# 15 Feb 2019 Unescaped left brace is now deprecated

use English;
use strict;
use utf8;
use warnings;

use Carp;
use Data::Dumper;
use Date::Parse;
use File::Basename qw(dirname);
use File::Spec;
use POSIX qw(strftime);

use FindBin qw($Bin);
use lib qw($Bin $Bin/MilHist);

use Cred;
use MilHist::ArticleHistory;
use MilHist::Bot;
use MilHist::Parser;
use MilHist::Template;
use MilHist::Template::Parameter;

binmode (STDERR, ':utf8');
binmode (STDOUT, ':utf8');

# Pages used
my $fa_page = "Wikipedia:Featured articles";
my $fanmp_page = "Wikipedia:Featured articles that haven't been on the Main Page";

my $cred = new Cred ();
my $editor = MilHist::Bot->new ($cred) or
  die "new MediaWiki::Bot failed";

sub date_matches ($$) {
	my ($d1, $d2) = @ARG;
	my $dt1 = str2time ($d1);
	my $dt2 = str2time ($d2);
	return $dt1 == $dt2;
}

sub todays_featured_article () {
  	my $date = strftime "%B %d, %Y", gmtime;
  	$date =~ s/  / /;
  	$date =~ s/0(\d,)/$1/;
  	my $tfa = "Template:TFA title/$date";
  	my $todays_page = $editor->fetch ($tfa);

  	$cred->showtime ("Todays' page is $todays_page\n");

  	my $talk = "Talk:$todays_page";
  	my $date2 = strftime "%d %B %Y", gmtime;
  	$date2 =~ s/0(\d )/$1/;

  	my $has_maindate = 0;
  	my $text = $editor->fetch ($talk);
  	my $parser = new MilHist::Parser ('text' => $text);
    my $article_history = new MilHist::ArticleHistory ('parser' => $parser);
  	my $template = $article_history->template ();
  	my @parameters = $template->find ('maindate\d*');
    foreach my $parameter (@parameters) {
        if (date_matches ($parameter->value (), $date2)) {
            $cred->showtime ("\tmaindate already updated manually\n");
            $has_maindate = 1;
        }
	  }

	  if (! $has_maindate) {
		    $cred->showtime ("\tupdating article history\n");
	      	my $id = @parameters ? 1 + scalar @parameters : '';
    		$article_history->add ("maindate$id" => "$date2\n");
    		$text = $parser->text ();

    		$editor->edit ({
    			page => $talk,
    			text => $text,
    			summary => "$todays_page is today's featured article",
    			bot => 1,
    			minor => 0,
    		}) or
    			$editor->error ("unable to edit '$talk'");
    }

	  return $todays_page;
}

eval {
    # Update today's featured article
    my $todays_page = todays_featured_article ();

    # First, we need to get the FA page
    my $fa_text = $editor->fetch ($fa_page);

    # Update today's masterpiece
    my $update = 0;
    if ($fa_text =~ /\{\{FA\/BeenOnMainPage\|'*\[\[\Q$todays_page\E\]\]'*/s) {
    	$cred->showtime ("\tBeenOnMainPage already updated manually\n");
    } elsif ($fa_text =~ /\{\{FA\/BeenOnMainPage\|'*\[\[\Q$todays_page\E\|.+?\]\]'*/s) {
    	$cred->showtime ("\tBeenOnMainPage already updated manually\n");
    } elsif ($fa_text =~ s/('*\[\[\Q$todays_page\E\]\]'*)/\{\{FA\/BeenOnMainPage|$1\}\}/s) {
    	$update = 1;
    } elsif ($fa_text =~ s/('*\[\[\Q$todays_page\E\|.+?\]\]'*)/\{\{FA\/BeenOnMainPage|$1\}\}/s) {
    	$update = 1;
    }

    if ($update) {
    	$cred->showtime ("\tupdating $fa_page\n");
    	$editor->edit ({
    		page => $fa_page,
    		text => $fa_text,
    		summary => "$todays_page is today's featured article",
    		bot => 1,
    		minor => 0,
    	}) or
    		$editor->error ("unable to edit '$fa_page'");
    }

    my @fanmp;
    my @a = split /\n/, $fa_text;
    foreach (@a) {
    	if (/^==/) {
    		push @fanmp, $ARG;
    	} elsif (/\[\[(File|Category):/) {
    		next;
    	} elsif (/\* "*'*\[\[[^#].+\]\]'*"*/) {
    		push @fanmp, $ARG;
    	} elsif (/^"*'*\[\[[^#].+\]\]'*"*/) {
    		push @fanmp, "\* $ARG";
    	}
    }

    # Now we insert the counts
    my $first = 1;
    my $count = 0;
    my $total = 0;
    my @lines;
    foreach my $line (@fanmp) {
    	if ($line =~ /^==/) {
    		if ($first) {
    			$first = 0;
    		} else {
    			if (0 == $count) {
    				push @lines, "* '''None'''";
    			}
    			push @lines, "<!-- $count -->";
    			$total += $count;
    			$count = 0;
    		}
    	} else {
    #			if (0 == $count) {
    #				$line =~ s/..//;
    #			}
    			++$count;
    	}
    	push @lines, $line;
    }
    push @lines, "<!-- $count -->\n";
    $total += $count;
    # print "total = $total\n";

    my $lines = join "\n", @lines;

    # Then we need to get the FANMP page
    my $fanmp_text = $editor->fetch ($fanmp_page);

    $fanmp_text =~ s/==.+\|}/|}/s;
    $fanmp_text =~ s/\{\{formatnum:\d+\}\}/\{\{formatnum:$total\}\}/;
    $fanmp_text =~ s/\|}/$lines|}/;

    # Update the FANMP page
    $editor->edit ({
    	page => $fanmp_page,
    	text => $fanmp_text,
    	summary => "Updating $fanmp_page to remove $todays_page",
    	bot => 1,
    	minor => 0,
    }) or
    	$editor->error ("unable to edit $fanmp_page");

};
if ($EVAL_ERROR) {
	$cred->error ($EVAL_ERROR);
}
$cred->showtime ("finished\n");
exit 0;