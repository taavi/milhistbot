#!/usr/bin/perl -w
#
# awards.pl -- Give out awards to worth MILHIST members
# Usage: awards.pl
#
# 19 Feb 2018 Created
# 17 Mar 2018 Correct typo
# 18 Jun 2018 Use new MilHist::Bot wrapper
#  7 Jul 2018 Use MilHist::Parser
# 20 Jul 2018 Correct some date-related problems
# 12 Oct 2018 Use DateTime more cleverly
# 13 Feb 2019 Use MilHist::Section
# 15 Feb 2019 Unescaped left brace is now deprecated
# 29 Apr 2019 Awards in priority order

use English;
use strict;
use utf8;
use warnings;

use Data::Dumper;
use DateTime;
use FindBin qw($Bin);
use lib qw($Bin $Bin/MilHist);

use Cred;
use MilHist::Bot;
use MilHist::Coordinators;
use MilHist::Parser;
use MilHist::Section;
use MilHist::Template;

my $dt         = DateTime->today ();
my $this_month = $dt->strftime ("%B %Y");
my $next       = DateTime->new ('day' => 1, 'month' => $dt->month (), 'year' => $dt->year ())->add ('months' => 1);
my $next_month = $next->strftime ("%B %Y");

my $nominations_page = 'Wikipedia talk:WikiProject Military history/Awards';

binmode (STDOUT, ':utf8');
binmode (STDERR, ':utf8');

my $cred   = new Cred ();
my $editor = MilHist::Bot->new ($cred) or
  die 'new MediaWiki::Bot failed';
my $coordinators = new MilHist::Coordinators ('editor' => $editor);


my $is_review_award = {
    'WikiChevrons'                                         => 1,
    'The Milhist reviewing award (1 stripe)'               => 1,
    'The Milhist reviewing award (2 stripes)'              => 1,
    'The Milhist reviewing award (3 stripes)'              => 1,
    'The Content Review Medal of Merit (Military history)' => 1,
};

sub notify_recipient ($) {
    my $award          = shift;
    my $user_talk_page = 'User talk:' . $award->{nominee};
    my $user_talk_text = $editor->fetch ($user_talk_page);

    my $templates = {
        'A-Class medal'                 => 'WPMILHIST A-Class medal',
        'A-Class medal with Oak Leaves' => 'WPMILHIST A-Class medal (Oakleaves)',
        'A-Class medal with Swords'     => 'WPMILHIST A-Class medal (Swords)',
        'A-Class medal with Diamonds'   => 'WPMILHIST A-Class medal (Diamonds)',

        'A-Class cross'                 => 'WPMILHIST A-Class cross',
        'A-Class cross with Oak Leaves' => 'WPMILHIST A-Class cross with Oak Leaves',
        'A-Class cross with Swords'     => 'WPMILHIST A-Class cross with Swords',
        'A-Class cross with Diamonds'   => 'WPMILHIST A-Class cross with Diamonds',

        'WikiChevrons'                                         => 'WPMILHIST WikiChevrons',
        'WikiChevrons with Oak Leaves'                         => 'WPMILHIST WikiChevrons with Oak Leaves',
        'The Milhist reviewing award (1 stripe)'               => 'WPMILHIST Service (review) 1 stripe',
        'The Milhist reviewing award (2 stripes)'              => 'WPMILHIST Service (review) 2 stripes',
        'The Milhist reviewing award (3 stripes)'              => 'WPMILHIST Service (review) 3 stripes',
        'The Content Review Medal of Merit (Military history)' => 'WPMILHIST Service (review) Content Review Medal of Merit',
    };

    my $template = $templates->{$award->{award}} or
      $cred->error ("unknown award: '$award->{award}'\n");
    my $approver_template = new MilHist::Template ('name' => 'user0');
    my $approver_parameter = $approver_template->add (1 => $award->{approval});
    $approver_parameter->default(1);
    
    unless ($award->{citation} =~ /\.$/) {
        $award->{citation} .= '.';
    }   

    my $award_template = new MilHist::Template ('name' => 'subst:' . $template);
    my $award_template_text = join ' ', 'On behalf of the Military History Project, I am proud to present the',
        $award->{award}, 'for', $award->{citation}, $approver_template->text, 'via ~~~~';
    my $award_parameter = $award_template->add (1 => $award_template_text);
    $award_parameter->default(1);

    my $section_text = $award_template->text . "\n";
    my $section      = new MilHist::Section (
        'name'  => 'Congratulations from the Military History Project',
        'text'  => $section_text,
        'level' => 2
    );

    $user_talk_text .= "\n" . $section->text;

    $cred->showtime ("\tAdding award to $user_talk_page\n");
    $editor->edit (
        {
            page    => $user_talk_page,
            text    => $user_talk_text,
            summary => "Awarded $award->{award} to $award->{nominee}",
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$user_talk_page'");

    my @history = $editor->get_history ($user_talk_page, 2) or
      $cred->error ("Unable get history of '$user_talk_page'");
    $award->{diff}  = $history[0]->{revid};
    $award->{oldid} = $history[1]->{revid};
}

sub add_to_historical_list ($) {
    my $award = shift;

    sub entry ($) {
        my $award = shift;
        return
"* [[User:$award->{nominee}|$award->{nominee}]]: for $award->{citation} (Awarded \{\{diff|page=User talk:$award->{nominee}|diff=$award->{diff}|oldid=$award->{oldid}|label=$this_month\}\})";
    }

    my $award_pages = {

        'WikiChevrons with Oak Leaves'  => 'Wikipedia:WikiProject Military history/Awards/OAK',

        'A-Class medal'                 => 'Wikipedia:WikiProject Military history/Awards/ACM',
        'A-Class medal with Oak Leaves' => 'Wikipedia:WikiProject Military history/Awards/ACM',
        'A-Class medal with Swords'     => 'Wikipedia:WikiProject Military history/Awards/ACM',
        'A-Class medal with Diamonds'   => 'Wikipedia:WikiProject Military history/Awards/ACM',

        'A-Class cross'                 => 'Wikipedia:WikiProject Military history/Awards/ACC',
        'A-Class cross with Oak Leaves' => 'Wikipedia:WikiProject Military history/Awards/ACC',
        'A-Class cross with Swords'     => 'Wikipedia:WikiProject Military history/Awards/ACC',
        'A-Class cross with Diamonds'   => 'Wikipedia:WikiProject Military history/Awards/ACC',
    };

    my $awards_page = $award_pages->{$award->{award}} or
      $cred->error ("Unable to find award '$award->{award}'");
    my $awards_text = $editor->fetch ($awards_page);

    if ($award->{award} =~ /A-Class/) {
        my @output;
        my $added         = 0;
        my $blank_lines   = 0;
        my $found_section = 0;
        my @award_lines   = split /\n/, $awards_text;
        foreach my $award_line (@award_lines) {
            if (!$added) {
                if ($found_section) {
                    if ($award_line eq '') {
                        if ($blank_lines) {
                            push @output, entry ($award);
                            $added = 1;
                        } else {
                            $blank_lines = 1;
                        }
                    } elsif ($award_line =~ /User:(.+?)\|/) {
                        if ($1 gt $award->{nominee}) {
                            push @output, entry ($award);
                            $added = 1;
                        } else {
                            $blank_lines = 1;
                        }
                    }
                } elsif ($award_line =~ /The ''$award->{award}'' was introduced/) {
                    $found_section = 1;
                }
            }
            push @output, $award_line;
        }
        if (!$added) {
            push @output, entry ($award);
        }
        $awards_text = join "\n", @output;
    }

    if ($award->{award} eq 'WikiChevrons with Oak Leaves') {
        my @output;
        my %awardee;
        my $added         = 0;
        my @award_lines   = split /\n/, $awards_text;
        foreach my $award_line (@award_lines) {
            if ($award_line =~ /User:(.+?)\|/) {
                if (!$added && $1 gt $award->{nominee}) {
                    push @output, entry ($award);
                    $awardee{$award->{nominee}}++;
                    $added = 1;
                }
                $awardee{$1}++;
            }
            push @output, $award_line;
        }
        if (!$added) {
            push @output, entry ($award);
            $awardee{$award->{nominee}}++;
        }           
        $awards_text = join "\n", @output;
        
        my $count = 0;
        my $twice = 0;
        foreach my $user (keys %awardee) {
            $count++;
            $twice++ if $awardee{$user} > 1;        
        }
                
        $awards_text =~ s/
To date, '''(\d+)''' individuals have been awarded the ''WikiChevrons with Oak Leaves''.  One award has been made posthumously, (\w+) individuals have received the award twice, and one award has been made to a group of editors collectively./
To date, '''$count''' individuals have been awarded the ''WikiChevrons with Oak Leaves''.  One award has been made posthumously, $twice individuals have received the award twice, and one award has been made to a group of editors collectively./;
    }
    
    $cred->showtime ("\tAdding award to $awards_page\n");
    $editor->edit (
        {
            page    => $awards_page,
            text    => $awards_text,
            summary => "Awarded $award->{award} to $award->{nominee}",
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$awards_page'");
}

sub add_to_bugle ($) {
    my $award = shift;

    my $bugle_template = join "\n",
      "\{\{Wikipedia:WikiProject Military history/News/$next_month/Header\}\}",
      '__NOTOC__',
      "\{\{WPMILHIST Newsletter section header 2|From the editors\}\}",
      '{| style="float: right; margin-left: 1em; margin-bottom: 1em; background: transparent;',
      '|- style="vertical-align:top;text-align: center;"',
'| [[File:US-O11 insignia.svg|40x40px]]&nbsp;[[File:US-O12 insignia.svg|40x40px]]<br/>[[File:Milhist coordinator emeritus.svg|40x40px]]',
      '|}',                                                               '',
      "\{\{WPMILHIST Newsletter section header 2|Awards and honours\}\}", '',
      "\{\{WPMILHIST Newsletter section header 2|Contest department\}\}",
      '{| style="float: right; margin-left: 1em; margin-bottom: 1em; background: transparent;"',
      '|- style="vertical-align:top;text-align: center;"',
      "| [[File:WikiChevrons.png|40x40px]]\&nbsp;[[File:Writer's barnstar.png|40x40px]]",
      '|}', '',
      "\{\{Wikipedia:WikiProject Military history/News/$next_month/Footer\}\}";

    my $bugle_award_text = {
        'A-Class medal' =>
"[[File:WPMH ACR.PNG||40x40px|right]]\n*The [[Wikipedia:WikiProject_Military_history/Awards#A-Class_medals|''A-Class medal'']]",
        'A-Class medal with Oak Leaves' =>
"[[File:WPMH ACR (Oakleaves).png||40x40px|right]]\n*The [[Wikipedia:WikiProject_Military_history/Awards#A-Class_medals|''A-Class medal with Oak Leaves'']]",
        'A-Class medal with Swords' =>
"[[File:WPMH ACR (Swords).png||40x40px|right]]\n*The [[Wikipedia:WikiProject_Military_history/Awards#A-Class_medals|''A-Class medal with Swords'']]",
        'A-Class medal with Diamonds' =>
"[[File:WPMH ACR (Diamonds).png||40x40px|right]]\n*The [[Wikipedia:WikiProject_Military_history/Awards#A-Class_medals|''A-Class medal with Diamonds'']]",

        'A-Class cross' =>
"[[File:WPMH ACR 2.png||40x40px|right]]\n*The [[Wikipedia:WikiProject_Military_history/Awards#A-Class_crosses|''A-Class cross'']]",
        'A-Class cross with Oak Leaves' =>
"[[File:WPMH ACR (Oakleaves) 2.png||40x40px|right]]\n*The [[Wikipedia:WikiProject_Military_history/Awards#A-Class_crosses|''A-Class cross with Oak Leaves'']]",
        'A-Class cross with Swords' =>
"[[File:WPMH ACR (Swords) 2.png||40x40px|right]]\n*The [[Wikipedia:WikiProject_Military_history/Awards#A-Class_crosses|''A-Class cross with Swords'']]",
        'A-Class cross with Diamonds' =>
"[[File:WPMH ACR (Diamonds) 2.png||40x40px|right]]\n*The [[Wikipedia:WikiProject_Military_history/Awards#A-Class_crosses|''A-Class cross with Diamonds'']]",

        'WikiChevrons' =>  
"[[File:WikiChevrons.svg||40x40px|right]]\n*The [[Wikipedia:WikiProject_Military_history/Awards#WikiChevrons|''WikiChevrons'']]",
        'WikiChevrons with Oak Leaves' =>  
"[[File:WikiChevronsOakLeaves.png||40x40px|right]]\n*The [[Wikipedia:WikiProject_Military_history/Awards#WikiChevrons with Oak Leaves|''WikiChevrons with Oak Leaves'']]",

    };

	my $award_priority = {
		'A-Class medal' 				=> 1,
		'A-Class medal with Oak Leaves' => 2,
		'A-Class medal with Swords'     => 3,
		'A-Class medal with Diamonds' 	=> 4,

		'A-Class cross' 				=> 5,
		'A-Class cross with Oak Leaves' => 6,
		'A-Class cross with Swords'     => 7,
		'A-Class cross with Diamonds' 	=> 8,
	};

    my $bugle_page = join '/', 'Wikipedia:WikiProject Military history', 'News', $next_month, 'Project news';
    my $bugle_text = $editor->get_text ($bugle_page) // $bugle_template;
    my $award_text = $bugle_award_text->{$award->{award}} or
        $cred->error ("no bugle text for '$award->{award}' in '$bugle_page'");

    my $lineno = 1;
    my @bugle_text = split /\n/, $bugle_text;
    my $bugle_award_header = "$award_text has been awarded to:";
    my $bugle_award_line = "** $award->{nominee} for $award->{citation}.";

    foreach (@bugle_text) {
        if (/(\{\{WPMILHIST Newsletter section header 2\|Contest department\}\})/) {
            splice @bugle_text, $lineno-1, 0, $bugle_award_header, $bugle_award_line, '';
            last;
        }
        if (/''(.+)''\]\] has been awarded to:/) {
            if ($1 eq $award->{award}) {
                splice @bugle_text, $lineno, 0, $bugle_award_line;
                last;
            } elsif ($award_priority->{$1} > $award_priority->{$award->{award}}) {
                splice @bugle_text, $lineno-2, 0, $bugle_award_header, $bugle_award_line, '';
                last;
            }
        }
        ++$lineno;
    }

    $bugle_text = join "\n", @bugle_text, '';
    $cred->showtime ("\tAdding award to $bugle_page\n");
    $editor->edit (
        {
            page    => $bugle_page,
            text    => $bugle_text,
            summary => "Awarded $award->{award} to $award->{nominee}",
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$bugle_page'");
}

sub update_nomination ($$$$) {
    my ($parser, $nominations_page, $nomination, $award) = @ARG;

    $nomination->add ('status' => 'awarded');
    $nomination->add ('diff'   => $award->{diff});
    $nomination->add ('oldid'  => $award->{oldid});
    my $nominations_text = $parser->text ();

    $cred->showtime ("\tUpdating nomination\n");
    $editor->edit (
        {
            page    => $nominations_page,
            text    => $nominations_text,
            summary => "Awarded $award->{award} to $award->{nominee}",
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$nominations_page'");
}

sub whodunnit ($) {
    my ($award) = @ARG;
    my $user;
    my @history = $editor->get_history ($nominations_page) or
      $editor->error ("unable to get history of $nominations_page'");
  HISTORY: foreach my $history (@history) {

        #   print "fetching revision $history->{revid} from $history->{timestamp_time} on $history->{timestamp_date}\n";
        my $nominations_text = $editor->get_text ($nominations_page, $history->{revid}) or
          $editor->error ("unable to find '$nominations_page' revid $history->{revid}");
        my $parser      = new MilHist::Parser ('text' => $nominations_text);
        my @nominations = $parser->find       ('WPMILHIST Award nomination');
      NOMINATION: foreach my $nomination (@nominations) {
            my $nominee  = $nomination->get ('nominee');
            my $awarded  = $nomination->get ('award');
            my $citation = $nomination->get ('citation');
            next NOMINATION
              unless ($nominee eq $award->{'nominee'} && $awarded eq $award->{'award'} && $citation eq $award->{'citation'});
            my $status = $nomination->get ('status');

            #           print "found $nominee $status\n";
            $user = $history->{user} if ($status eq 'approved');
            last HISTORY if ($status eq 'nominated');
            next HISTORY;
        }
    }

    #	print "approved by user=", $user, "\n";
    die ("'$user' is NOT a ccoordinator\n") unless $coordinators->is_coordinator ($user);
    return $user;
}

$cred->showtime ("started\n");
my $nominations_text = $editor->fetch ($nominations_page);
my $parser      = new MilHist::Parser ('text' => $nominations_text);
my @nominations = $parser->find       ('WPMILHIST Award nomination');
foreach my $nomination (@nominations) {
	eval {
        my $status = $nomination->get ('status');
        if (defined $status && $status eq 'approved') {
            my $award = {
                'nominee'  => $nomination->get ('nominee'),
                'award'    => $nomination->get ('award'),
                'citation' => $nomination->get ('citation'),
            };
            $award->{'approval'} = whodunnit ($award);
            $cred->showtime ('Found ', $award->{'award'}, ' award for ', $award->{'nominee'}, ' approved by ',
                $award->{'approval'}, "\n");
            notify_recipient ($award);
            unless ($is_review_award->{$award->{'award'}}) {
                add_to_historical_list ($award);
                add_to_bugle           ($award);
            }
            update_nomination ($parser, $nominations_page, $nomination, $award);
        }
    };
	if ($EVAL_ERROR) {
	    $cred->warning ($EVAL_ERROR);
	}
}
$cred->showtime ("finished\n");
exit 0;
