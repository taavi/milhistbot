using System.Text.RegularExpressions;

public static class StringHelpers
{
    public static bool Matches (this string str, string pattern)
    {
    	return Regex.Match (str, pattern, RegexOptions.IgnoreCase).Success;
    } 
    
    public static string Quote (this string str)
    {
        return "\"" + str + "\"";
    }
    
    public static int WordsCount (this string str)
    {
        return Regex.Matches(str, @"((\w+(\s?)))").Count;
    }
    

}
