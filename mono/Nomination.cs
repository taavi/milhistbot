﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Wikimedia
{
	public class ArticlePage : Page
	{
		private readonly Nomination Nomination;
		private TalkPage? talk;
		public new TalkPage Talk
		{
			get
			{
				if (null == talk)
				{
					talk = new TalkPage(Nomination, "Talk:" + Title);
				}
				return talk;
			}
		}

		public ArticlePage(Nomination nomination, string title) : base(nomination.Bot, title)
		{
			Nomination = nomination;
		}
	}

	public class TalkPage : Page
	{
		private readonly Nomination Nomination;

		public void UpdateArticleHistory(string result)
		{
			Load();
			if (null == ArticleHistory)
			{
				ArticleHistory = new ArticleHistory(this);
			}

			DateTime date = DateTime.Now;
			string link = Title;
			int revid = ArticleHistory.GetRevId(Article, date);
			Milestone milestone = new Milestone(Nomination.Action, date.ToString("HH:mm:ss dd MMMM yyyy (UTC)"), link, revid, result);
			Article.Talk.ArticleHistory.Add(milestone);
		}

		public TalkPage(Nomination nomination, string title, int revid = -1) : base(nomination.Bot, title, revid)
		{
			Nomination = nomination;
		}
	}

	public class NominationPage : Page
	{
		private readonly Nomination Nomination;
		public readonly Template Template;
		private ArticlePage? article;

		public new ArticlePage Article
		{
			get
			{
				string? title;
				if (null == article)
				{
					Regex filter = new Regex(Nomination.Regex);
					var match = filter.Match(Title);
					if (match.Success)
					{
						title = match.Groups["title"].Value;
					}
					else
					{
						throw new Exception("Bad nomination  name: " + Title);
					}
					article = new ArticlePage(Nomination, title);
				}
				return article;
			}
		}

		public void Archive(string comment)
		{
			Load();
			Template archiveTop = new Template("article top");
			Insert(Top(), archiveTop, Token.Newline, new Token(comment), Token.Newline);
			Template archiveBottom = new Template("archive bottom");
			Add(archiveBottom, Token.Newline);
		}

		public NominationPage(Bot bot, Nomination nomination, Template template) : this(bot, nomination, template, template.Name)
		{
		}

		public NominationPage(Bot bot, Nomination nomination, Template template, string name) : base(bot, name)
		{
			Nomination = nomination;
			Template = template;
		}
	}

	public class NominationsPage : Page
	{
		private readonly Nomination Nomination;
		private readonly List<NominationPage> nominations;

		public List<NominationPage> Nominations
		{
			get
			{
				if (!nominations.Any())
				{
					Load();
					List<Template>? nominationTemplates = FindAll(Nomination.Regex);
					foreach (Template template in nominationTemplates)
					{
						NominationPage nominationPage = new NominationPage(Bot, Nomination, template);
						nominations.Add(nominationPage);
					}
				}
				nominations.Reverse();
				return nominations;
			}
		}

		public void Remove(NominationPage nomination)
		{
			Load();
			Remove(nomination.Template);
		}

		public NominationsPage(Nomination nomination) : base(nomination.Bot, nomination.Path)
		{
			Nomination = nomination;
			nominations = new List<NominationPage>();
		}
	}

	public class Nomination
	{
		public Bot Bot { get; }
		public string Action { get; set; }
		public string Path   { get; set; }
		public string Regex  { get; set; }

		public NominationsPage NominationsPage
		{
			get
			{
				return new NominationsPage(this);
			}
		}

		public Nomination(Bot bot)
		{
			Bot = bot;
			Path   = "";
			Action = "";
			Regex  = "";
		}
	}
}

