﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Wikimedia.Featured
{
	public class Featured: Nomination
	{

		public Featured(Bot bot): base(bot)
		{
			Action = "FAC";
			Path   = "Wikipedia:Featured article candidates";
			Regex  = @"Featured article candidates/(?<title>.+)/archive\d+";
		}
	}

	public class Review: Nomination
	{

		public Review(Bot bot):	base (bot)
		{
			Action = "FAR";
			Path   = "Wikipedia:Featured article review";
			Regex  = @"Featured article review/(?<title>.+)/archive\d+";
		}
	}

	public class List: Nomination
	{
		public List(Bot bot) : base(bot)
        {
			Action = "FLC";
			Path   = "Wikipedia:Featured list candidates";
			Regex  = @"Featured list candidates/(?<title>.+)/archive\d+";
		}
	}
}
