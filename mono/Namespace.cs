using System;
using System.Collections.Generic;

namespace Wikimedia
{
    public class Namespace : IToken
    {
        static private string[] namespaces = new string[]{  "Media", "Special", "Talk", "User", "User talk", "Project", "Project talk", "File", "File talk",
                                            "Image", "Image talk", "MediaWiki", "MediaWiki talk", "Template", "Template talk", "Help", "Help talk",
                                            "Category", "Category talk", "WP", "Wikipedia", "Wikipedia Talk"};

        static private Dictionary<string, string> mainspace2talk;
        static private Dictionary<string, string> talkspace2main;

        public string Text { get; }
        public TokenType Type { get; }

        public override string ToString()
        {
            return Text;
        }

        static public bool IsNamespace(string ns)
        {
            return Array.Exists(namespaces, element => element == ns);
        }

        public bool IsMainspace
        {
            get => mainspace2talk.ContainsKey(Text);
        }

        public bool IsTalkspace
        {
            get => talkspace2main.ContainsKey(Text);
        }

        public Namespace Mainspace
        {
            get
            {
                if (IsMainspace)
                {
                    return this;
                }
                else if (talkspace2main.ContainsKey(Text))
                {
                    return new Namespace(talkspace2main[Text]);
                }
                else
                {
                    throw new Exception("No main space for namespace " + Text);
                }
            }
        }

        public Namespace Talkspace
        {
            get
            {
                if (IsTalkspace)
                {
                    return this;
                }
                else if (mainspace2talk.ContainsKey(Text))
                {
                    return new Namespace(mainspace2talk[Text]);
                }
                else
                {
                    throw new Exception("No talk space for namespace " + Text);
                }
            }
        }

        static Namespace()
        {
            mainspace2talk = new Dictionary<string, string>()
            {
                { "", "Talk" },
                { "User", "User talk" },
                { "Project", "Project talk" },
                { "File", "File talk" },
                { "Image", "Image talk" },
                { "MediaWiki", "MediaWiki talk" },
                { "Template", "Template talk" },
                { "Help", "Help talk" },
                { "Category", "Category talk" },
                { "WP", "Wikipedia Talk" },
                { "Wikipedia", "Wikipedia Talk" },
            };
            talkspace2main = new Dictionary<string, string>()
            {
                { "Talk", ""  },
                { "User talk", "User"  },
                { "Project talk", "Project" },
                { "File talk", "File" },
                { "Image talk", "Image"  },
                { "MediaWiki talk", "MediaWiki"  },
                { "Template talk", "Template" },
                { "Help talk", "Help"  },
                { "Category talk", "Category"  },
                { "Wikipedia Talk", "Wikipedia"  },
            };
        }

        public Namespace(string name, bool colon = false)
        {
            Text = name;
            Type = TokenType.Namespace;
        }
    }
}