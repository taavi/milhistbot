using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Newtonsoft.Json.Linq;

namespace Wikimedia
{
    public class PageMissingException : BotException
    {
        public PageMissingException(string message) : base(message)
        {
        }
    }

    public class BotException : Exception
    {
        public BotException(string message) : base(message)
        {
        }

        public BotException(Cred cred, string message) : base(message)
        {
            cred.Warning(message);
            cred.Close();
        }
    }

    public class Bot
    {
        private Cred? cred;
        public  Cred Cred { get => (cred ??= new Cred ()); }
        private CookieAwareWebClient? client;
        private CookieAwareWebClient Client { get => (client ??= new CookieAwareWebClient()); }
        private string address = "https://en.wikipedia.org";
        private const string api = "/w/api.php";
        private string? editToken;

        public static bool AllowBots(string text, string job)
        {
            return !Regex.IsMatch(text, @"\{\{(nobots|bots\|(allow=none|deny=(?!none).*(" + job.Normalize() + @".*|all)|optout=all))\}\}", RegexOptions.IgnoreCase);
        }

        private string url(NameValueCollection nvc)
        {
            var array = (from key in nvc.AllKeys
                         from value in nvc.GetValues(key)!
                         select string.Format("{0}={1}", HttpUtility.UrlEncode(key), HttpUtility.UrlEncode(value)))
                .ToArray();
            string url = address + api + "?" + string.Join("&", array);
            //      Console.WriteLine(url);
            return url;
        }

        private void JasonContinue (JObject json, string key, Query query)
        {
            if (json.ContainsKey("continue"))
            {
                query.Start = JasonQuery(json, "continue", key);
                query.Continue = true;
             }
            else
            {
                query.Continue = false;
            }
            // Console.WriteLine ("continue=" + query.Continue);
        }

        private void JasonError (JObject json)
        {

            if (json.ContainsKey("error"))
            {
                string code = JasonQuery (json, "error", "code");
                string info = JasonQuery (json, "error", "info");
                throw new BotException(Cred, "Load failed: (" + code + "): " + info);
            }
        }

        private string JasonQuery(JObject json, string jquery, params string[] jstrings)
        {
            JToken? jtoken;
            jtoken = json[jquery];
            if (jtoken == null)
            {
                throw new BotException(jquery + " should not be null");
            }

            foreach (string jstring in jstrings)
            {
                jtoken = jtoken[jstring];
                if (jtoken == null)
                {
                    throw new BotException(jstring + " should not be null");
                }
            }
            return (string)jtoken!;
        }

        private string GetResponse (string url)
        {
            //      Console.WriteLine(url);
            string responseString;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                responseString = reader.ReadToEnd();
            }
            //      Console.WriteLine(responseString);
            return responseString;
        }

        private string getEditToken()
        {
            if (null == editToken)
            {
                NameValueCollection parameters = new NameValueCollection();
                parameters["action"] = "query";
                parameters["format"] = "json";
                parameters["meta"] = "tokens";
                string editTokenUrl = url(parameters);

                NameValueCollection values = new NameValueCollection();
                var response = Client.UploadValues(editTokenUrl, values);
                string responseString = Encoding.UTF8.GetString(response);
                //          Console.WriteLine(responseString);

                JObject json = JObject.Parse(responseString);
                editToken = JasonQuery(json, "query", "tokens", "csrftoken");
                //          Console.WriteLine(editToken);
            }
            return editToken;
        }

        public List<Page> Category(Query query)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["action"] = "query";
            parameters["list"] = "categorymembers";
            if (query.User == null)
            {
                throw new BotException("Query.User should not be null");
            }
            parameters["cmtitle"] = "category:" + query.User.Replace(' ', '_');
            parameters["cmlimit"] = query.Limit;
            if (null != query.Start)
            {
                parameters["cmcontinue"] = query.Start;
            }
            parameters["format"] = "json";
            parameters["formatversion"] = "2";
            string get_category_url = url(parameters);

            string responseString;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(get_category_url);
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                responseString = reader.ReadToEnd();
            }

            // Console.OutputEncoding = System.Text.Encoding.UTF8;
           	// Console.WriteLine(responseString);

            List<Page> pages = new List<Page>();
            JObject json = JObject.Parse(responseString);
            JasonError(json);

            JasonContinue(json, "cmcontinue", query);

            var members = json["query"]!["categorymembers"]!;
            foreach (var member in members)
            {
                string title = (string)member["title"]!;
                Page page = new Page(this, title);
                page.PageId = (int)member["pageid"]!;
                page.Ns = (int)member["ns"]!;
                pages.Add(page);
            }
            return pages;
        }

        public List<String> Categories(string title)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["action"] = "query";
            parameters["prop"] = "categories";
            parameters["titles"] = title;
            parameters["cllimit"] = "500";
            parameters["format"] = "json";
            parameters["formatversion"] = "2";
            string getCategoriesUrl = url(parameters);
            // Console.WriteLine(getCategoriesUrl);

            string responseString;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(getCategoriesUrl);
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                responseString = reader.ReadToEnd();
            }

            // Console.OutputEncoding = System.Text.Encoding.UTF8;
            // Console.WriteLine(responseString);

            JObject json = JObject.Parse(responseString);
            JasonError(json);

            List<String> categories = new List<string>();
            var members = json["query"]!["pages"]![0]!["categories"];
            if (null != members)
            {
                foreach (var member in members)
                {
                    var category = (string)member["title"]!;
                    categories.Add(category);
                }
            }
            return categories;
        }

        public List<Contribution> Contributions(Query query)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["action"] = "query";
            parameters["format"] = "json";
            parameters["list"] = "usercontribs";
            parameters["ucuser"] = query.User;
            parameters["uclimit"] = query.Limit;
            if (null != query.Start)
            {
                parameters["ucstart"] = query.Start.Substring(0, 14);
            }
            if (null != query.End)
            {
                parameters["ucend"] = query.End;
            }

            string userContributionsUrl = url(parameters);
            //      Console.WriteLine(userContributionsUrl);

            string responseString;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(userContributionsUrl);
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                responseString = reader.ReadToEnd();
            }
            //      Console.WriteLine(responseString);

            JObject json = JObject.Parse(responseString);
            JasonError(json);

            JasonContinue(json, "uccontinue", query);

            List<Contribution> userContributions = new List<Contribution>();
            var contributions = json["query"]!["usercontribs"]!;
            foreach (var contribution in contributions)
            {
                var user = (string)contribution["user"]!;
                var pageId = (int)contribution["pageid"]!;
                var revId = (int)contribution["revid"]!;
                var parentId = (int)contribution["parentid"]!;
                var namespaceId = (int)contribution["ns"]!;
                var timestamp = (DateTime)contribution["timestamp"]!;
                var comment = (string)contribution["comment"]!;
                var size = (int)contribution["size"]!;
                Contribution c = new(this, user, pageId, revId, parentId, namespaceId, timestamp, comment, size);
                userContributions.Add(c);
            }

            return userContributions;
        }

        public List<Page> Embedded(Query query)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["action"] = "query";
            parameters["format"] = "json";
            parameters["list"] = "embeddedin";
            parameters["eititle"] = query.Title;
            if (query.Continue)
            {
                parameters["eicontinue"] = query.Start;
            }
            string embeddedUrl = url(parameters);
            //      Console.WriteLine(embeddedUrl);

            string responseString;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(embeddedUrl);
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                responseString = reader.ReadToEnd();
            }
            //      Console.WriteLine(responseString);

            List<Page> pages = new List<Page>();
            JObject json = JObject.Parse(responseString);
            JasonError(json);

            JasonContinue(json, "eicontinue", query);

            var backlinks = json["query"]!["embeddedin"]!;
            foreach (var backlink in backlinks)
            {
                string title = (string)backlink["title"]!;
                Page page = new Page(this, title);
                page.PageId = (int)backlink["pageid"]!;
                page.Ns = (int)backlink["ns"]!;
                pages.Add(page);
            }
            return pages;
        }

        public List<Revision> History(Query query)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["action"] = "query";
            parameters["prop"] = "revisions";
            parameters["titles"] = query.Title;
            parameters["rvprop"] = "ids|timestamp|user|comment";
            parameters["rvslots"] = "main";
            parameters["rvlimit"] = query.Limit.ToString();
            if (null != query.Start)
            {
                parameters["rvstart"] = query.Start.Substring(0, 14);
            }
            if (null != query.End)
            {
                parameters["rvend"] = query.End;
            }
            parameters["format"] = "json";
            parameters["formatversion"] = "2";
            string get_history_url = url(parameters);
            //      Console.WriteLine (get_history_url);

            string responseString;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(get_history_url);
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                responseString = reader.ReadToEnd();
            }
            //      Console.WriteLine(responseString);

            JObject json = JObject.Parse(responseString);
            JasonError(json);

            JasonContinue(json, "rvcontinue", query);

            List<Revision> history = new List<Revision>();
            if (null != json["query"]!["pages"]![0]!["missing"])
            {
                throw new PageMissingException("History failed: page '" + query.Title + "' missing");
            }

            var revisions = json["query"]!["pages"]![0]!["revisions"]!;
            foreach (var revision in revisions)
            {
                Revision r = new Revision();
                r.Title = query.Title!;
                r.RevId = (int)revision["revid"]!;
                r.ParentId = (int)revision["parentid"]!;
                r.User = (string)revision["user"]!;
                r.Timestamp = (DateTime)revision["timestamp"]!;
                r.Comment = (string)revision["comment"]!;
                history.Add(r);
            }
            return history;
        }

        public void Load(Page page, int revid)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["action"] = "query";
            parameters["prop"] = "revisions";
            parameters["rvprop"] = "content";
            parameters["rvslots"] = "main";
            if (revid > 0)
            {
                parameters["rvstartid"] = revid.ToString();
                parameters["rvendid"] = revid.ToString();
            }
            parameters["format"] = "json";
            parameters["formatversion"] = "2";
            parameters["titles"] = page.Title.Replace(" ", "_");
            string query_url = url(parameters);
            // Console.WriteLine(query_url);

            string responseString = "";
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(query_url);
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    responseString = reader.ReadToEnd();
                }
            }
            catch (WebException wex)
            {
                if (wex.Message.Equals("The operation has timed out."))
                {
                    throw new TimeoutException();
                }
            }
            //Console.OutputEncoding = System.Text.Encoding.UTF8;
            //Console.WriteLine(responseString);

            JObject json = JObject.Parse(responseString);
            JasonError(json);

            var jpage = json["query"]!["pages"]![0]!;
            if (null != jpage["invalidreason"])
            {
                throw new PageMissingException("Load (" + page.Title + ") failed: " + (string)jpage["invalidreason"]!);
            }

            if (null != jpage["missing"])
            {
                throw new PageMissingException("Load (" + page.Title + ") failed: page missing");
            }

            page.PageId = (int)jpage["pageid"]!;
            page.Ns = (int)jpage["ns"]!;
            page.Content = (string)jpage["revisions"]![0]!["slots"]!["main"]!["content"]!;
            page.ReadOnly = !AllowBots(page.Content, Cred.Job);
        }

        private string loginToken()
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["action"] = "query";
            parameters["format"] = "json";
            parameters["meta"] = "tokens";
            parameters["type"] = "login";
            string loginTokenUrl = url(parameters);

            NameValueCollection values = new NameValueCollection();
            var response = Client.UploadValues(loginTokenUrl, values);
            string responseString = Encoding.UTF8.GetString(response);
            //      Console.WriteLine(responseString);

            JObject json = JObject.Parse(responseString);
            string logintoken = JasonQuery(json, "query", "tokens", "logintoken");
            //      Console.WriteLine(logintoken);
            return logintoken;
        }

        private void login(string name, string password)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["action"] = "login";
            parameters["format"] = "json";
            parameters["lgname"] = name;
            string loginUrl = url(parameters);

            NameValueCollection values = new NameValueCollection();
            values["lgpassword"] = password;
            values["lgtoken"] = loginToken();
            var response = Client.UploadValues(loginUrl, values);
            string responseString = Encoding.UTF8.GetString(response);
            //      Console.WriteLine(responseString);

            JObject json = JObject.Parse(responseString);
            string result = JasonQuery(json, "login", "result");
            //      Console.WriteLine(result);

            if (! "Success".Equals(result))
            {
                throw new BotException(Cred, "Login failed: " + responseString);
            }
        }

        public string Prediction(string title)
        {
            Query query = new Query(title, 1);
            var history = History(query);
            int revid = history[0].RevId;
            string ores_url = "http://ores.wikimedia.org/v3/scores/enwiki/?models=wp10&revids=" + revid;
            //      Console.WriteLine("query=" + ores_url);

            string responseString;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ores_url);
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                responseString = reader.ReadToEnd();
            }
            //      Console.WriteLine("response=" + responseString);

            JObject json = JObject.Parse(responseString);
            string prediction = JasonQuery(json, "enwiki", "scores", revid.ToString(), "wp10", "score", "prediction");
            return prediction;
        }

        public List<Revision> RecentChanges(Query query)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["action"] = "query";
            parameters["list"] = "recentchanges";
            parameters["action"] = "query";
            parameters["rcprop"] = "title|ids|timestamp|user|comment";
            parameters["rclimit"] = query.Limit.ToString();
            if (! query.Namespace.Text.Equals(""))
            {
                parameters["rcnamespace"] = query.Namespace.Text;
            }
            if (null != query.User)
            {
                parameters["rcuser"] = query.User;
            }
            if (null != query.Start)
            {
                parameters["rcstart"] = query.Start.Substring(0, 14);
            }
            if (null != query.End)
            {
                parameters["rcend"] = query.End;
            }
            if (query.Continue)
            {
                parameters["rccontinue"] = query.Start;
            }
            parameters["format"] = "json";
            parameters["formatversion"] = "2";
            string get_recent_changes_url = url(parameters);
            // Console.WriteLine (get_recent_changes_url);

            string responseString;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(get_recent_changes_url);
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                responseString = reader.ReadToEnd();
            }
            // Console.WriteLine(responseString);

            JObject json = JObject.Parse(responseString);
            JasonError(json);

            JasonContinue(json, "rccontinue", query);
            
            List<Revision> recentChanges = new List<Revision>();
            var changes = json["query"]!["recentchanges"]!;
            foreach (var change in changes)
            {
                Revision r = new Revision();
                r.Title = (string)change["title"]!;
                r.RevId = (int)change["revid"]!;
                r.User = (string)change["user"]!;
                r.Timestamp = (DateTime)change["timestamp"]!;
                r.Comment = (string)change["comment"]!;
                recentChanges.Add(r);
            }

            return recentChanges;
        }

        public List<Page> Redirects(Query query)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["action"] = "query";
            parameters["prop"] = "redirects";
            parameters["format"] = "json";
            parameters["titles"] = query.Title;
            parameters["rdlimit"] = query.Limit;
            if (query.Continue)
            {
                parameters["rdcontinue"] = query.Start;
            }
            string redirectsUrl = url(parameters);
            // Console.WriteLine(redirectsUrl);

            string responseString;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(redirectsUrl);
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                responseString = reader.ReadToEnd();
            }
            // Console.WriteLine(responseString);

            List<Page> pages = new List<Page>();
            JObject json = JObject.Parse(responseString);
            JasonError(json);

            JasonContinue(json, "rdcontinue", query);

            var t = json["query"]!["pages"]!;
            foreach (JProperty jproperty in t)
            {
                string pageId = jproperty.Name;
                var redirects = json["query"]!["pages"]![pageId]!["redirects"];
                if (null != redirects)
                {
                    foreach (var redirect in redirects)
                    {
                        string title = (string)redirect["title"]!;
                        Page page = new Page(this, title);
                        page.PageId = (int)redirect["pageid"]!;
                        page.Ns = (int)redirect["ns"]!;
                        pages.Add(page);
                    }
                }
            }
            return pages;
        }

        public Page? RedirectsTo(string title)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["action"] = "query";
            parameters["prop"] = "redirects";
            parameters["format"] = "json";
            parameters["titles"] = title;
            parameters["redirects"] = "";
            string redirectsToUrl = url(parameters);
            // Console.WriteLine(redirectsToUrl);

            string responseString = GetResponse(redirectsToUrl);
            JObject json = JObject.Parse(responseString);
            JasonError(json);

            Page? redirectsTo = null;
            var query = json["query"];
            if (null == query)
            {
                throw new BotException("Query muist not be null")
;            }
            var redirects = json["query"]!["redirects"];
            if (null != redirects)
            {
                foreach (var redirect in redirects)
                {
                    if (null != redirect)
                    {
                        var to = redirect["to"];
                        if (null != to)
                        {
                            redirectsTo = new Page(this, (string)to!);
                        }
                    }
                }
            }

            return redirectsTo;
        }

        public void Save(Page page, string summary)
        {
            if (page.ReadOnly)
            {
                throw new BotException(Cred, "Page does not allow bot update");
            }

            NameValueCollection parameters = new NameValueCollection();
            parameters["action"] = "edit";
            parameters["title"] = page.Title;
            parameters["format"] = "json";
            parameters["bot"] = "true";
            parameters["summary"] = summary;
            string edit_url = url(parameters);

            NameValueCollection headers = new NameValueCollection();

            NameValueCollection values = new NameValueCollection();
            values["token"] = getEditToken();

            NameValueCollection multiparts = new NameValueCollection();
            multiparts["text"] = page.Content;

            CookieAwareWebClient.UploadResponse uploadResponse = Client.Upload(edit_url, headers, values, multiparts);
            string responseString = uploadResponse.ResponseBody;

            // Console.WriteLine(responseString);

            JObject json = JObject.Parse(responseString);
            JasonError(json);

            var jedit = json["edit"];
            string result = JasonQuery (json, "edit", "result");
            // Console.WriteLine(result);

            if (!"Success".Equals(result))
            {
                throw new BotException(Cred, "edit failed: " + responseString);
            }

            if (null != jedit!["oldrevid"])
            {
                var oldrevid = jedit!["oldrevid"];
                page.OldRevId = (int)oldrevid!;
            }
            if (null != jedit!["newrevid"])
            {
                var newrevid = jedit!["newrevid"];
                page.RevId = (int)newrevid!;
            }
        }

        public void Save(Section section, string summary)
        {
            if (section.Page.ReadOnly)
            {
                throw new BotException(Cred, "Page does not allow bot update");
            }

            NameValueCollection parameters = new NameValueCollection();
            parameters["action"] = "edit";
            parameters["title"] = section.Page.Title;
            parameters["section"] = section.Number;
            parameters["sectiontitle"] = section.Title;
            parameters["format"] = "json";
            parameters["bot"] = "true";
            parameters["summary"] = summary;
            string editUrl = url(parameters);

            NameValueCollection headers = new NameValueCollection();

            NameValueCollection values = new NameValueCollection();
            values["token"] = getEditToken();

            NameValueCollection multiparts = new NameValueCollection();
            multiparts["text"] = section.Contents.Text;

            CookieAwareWebClient.UploadResponse uploadResponse = Client.Upload(editUrl, headers, values, multiparts);
            string responseString = uploadResponse.ResponseBody;
            //      Console.WriteLine(responseString);

            JObject json = JObject.Parse(responseString);
            JasonError(json);
            string result = JasonQuery (json, "edit", "result");
            //      Console.WriteLine(result);

            if (!"Success".Equals(result))
            {
                throw new BotException(Cred, "edit failed: " + responseString);
            }
        }

        public string Title(int pageId)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["action"]  = "query";
            parameters["format"]  = "json";
            parameters["bot"]     = "true";
            parameters["pageids"] = pageId.ToString();
            string titleQueryUrl = url(parameters);
            string responseString = GetResponse(titleQueryUrl);
            JObject json = JObject.Parse(responseString);
            string title = JasonQuery(json, "query", "pages", pageId.ToString(), "title");

            return title;
        }

        public int Views(string title, DateTime date)
        {
            string date_string = date.ToString("yyyyMMdd00");
            string page_views_url = "https://wikimedia.org/api/rest_v1/metrics/pageviews/per-article/en.wikipedia/all-access/all-agents/" +
                    title.Replace(" ", "_") + "/daily/" + date_string + "/" + date_string;

            string responseString = GetResponse(page_views_url);
            JObject json = JObject.Parse(responseString);
            var items = json["items"];
            if (items == null)
            {
                throw new BotException("items should not be null");
            }

            var item = items[0];
            if (item == null)
            {
                throw new BotException("items[0] should not be null");
            }

            var views = item["views"];
            if (views == null)
            {
                throw new BotException("views should not be null");
            }
            
            return (int)views;
        }

        public void Close()
        {
            editToken = null;
            if (null != cred)
            {
                Cred.Close();
                cred = null;
            }
            if (null != client)
            {
                client.Dispose();
                client = null;
            }
        }

        public Bot()
        {
            try
            {
                cred = new Cred();
                editToken = null;
                client = new CookieAwareWebClient();
                login(Cred.User, Cred.Password);
            }
            catch (CredException cex)
            {
                throw new BotException(cex.Message);
            }
            catch (WebException wex)
            {
                throw new BotException(Cred, wex.Message);
            }
        }

    }
}
