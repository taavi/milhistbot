using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Xml;

public class CredException : Exception
{
    public Cred Cred {get; set; }

    public CredException(Cred cred, string message) : base(message)
    {
        Cred = cred;
    }
}

public class Cred {
    public string Job { get; set; }
    public string User { get; set; }
    public string Password { get; set; }
    public string LogsDirectory { get; set; }

    private StreamWriter? log;
    private List<string> mailList;
    private string smtpHost;

    public void mail (string from, string to, string message) {
        MailMessage mail = new MailMessage(from + "@tools.wmflabs.org", to);
        mail.Subject = "Error report";
        mail.Body = message;
        SmtpClient client = new SmtpClient();
        client.Host = smtpHost;
        client.Send(mail);
    }

    public string timestamp () {
        return DateTime.Now.ToString("HH:mm dd MMMM yyyy");
    }

    public void Warning (string message) {
        string t = timestamp () + " " + message;
        Console.Error.WriteLine (t);
        var log = Log ();
        log.WriteLine (t);
        log.Flush ();
        foreach (string to in mailList)
        {
            mail (User, to, message);
        }
    }

    public void Showtime (string message) {
        string t = timestamp () + " " + message;
        Console.WriteLine (t);
        var log = Log ();
        log.WriteLine (t);
        log.Flush ();
    }

    public StreamWriter Log ()
    {
        if (log == null)
        {
            string logFileName = Path.Combine (LogsDirectory, Job + ".log");
            if (! Directory.Exists(LogsDirectory))
            {
		        Directory.CreateDirectory(LogsDirectory);
            }
            log = new StreamWriter (logFileName, true, Encoding.UTF8);
        }
        return log;
    }

    public void Close ()
    {
        if (log != null)
        {
            log.Close ();
            log = null;
        }
    }

    public Cred() {
        // Get the credentials file
        LogsDirectory    = "/tmp";
        string fullname  = Environment.GetCommandLineArgs()[0];
        if (null == fullname)
        {
            throw new CredException(this, "unable to get fullname from command line");
        }
        Job = Path.GetFileNameWithoutExtension(fullname);
        string? pathname = Path.GetDirectoryName(fullname);
        if (null == pathname)
        {
            throw new CredException(this, "unable to get pathname for " + fullname);
        }
        string credx    = Path.Combine(pathname, "credx.xml");

        // Find the job + get the user
        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.Load(credx);
        XmlNode? rootNode = xmlDocument.DocumentElement;
        if (null == rootNode)
        {
            throw new CredException(this, "root node not found in " + credx);
        }
        XmlNode? userNode = rootNode.SelectSingleNode("descendant::job[name='" + Job + "']");
        if (null == userNode)
        {
            throw new CredException (this, "job " + Job + " not found in " + credx);
        }
        XmlNode? userElement = userNode.SelectSingleNode("user");
        if (null == userElement)
        {
            throw new CredException(this, "user for job " + Job + " not found in " + credx);
        }
        User = userElement.InnerText;

        //  Now we have the user, extract the other details
        XmlNode? credNode = rootNode.SelectSingleNode("descendant::cred[user='" + User + "']");
        if (null == credNode)
        {
            throw new CredException(this, "cred for user " + User + " not found in " + credx);
        }
        XmlNode? credElement = credNode.SelectSingleNode("password");
        if (null == credElement)
        {
            throw new CredException(this, "password for user " + User + " not found in " + credx);
        }
        Password = credElement.InnerText;

        XmlNodeList? mailNodeList = credNode.SelectNodes("descendant::mail");
        if (null == mailNodeList)
        {
            throw new CredException(this, "mail list for user " + User + " not found in " + credx);
        }
        mailList = new List<string>();
        foreach (XmlNode mailNode in mailNodeList) {
            XmlNode? mailElement = mailNode.SelectSingleNode("to");
            if (null == mailElement)
            {
                throw new CredException(this, "mail list to address for user " + User + " not found in " + credx);
            }
            mailList.Add (mailElement.InnerText);
        }

        XmlNode? logsNode = rootNode.SelectSingleNode("descendant::logs");
        if (null == logsNode)
        {
            throw new CredException(this, "logs node not found in " + credx);
        }
        XmlNode? logsElement = logsNode.SelectSingleNode("directory");
        if (null == logsElement)
        {
            throw new CredException(this, "logs directory not found in " + credx);
        }
        LogsDirectory = logsElement.InnerText;

        XmlNode? smtpHostNode = rootNode.SelectSingleNode("descendant::smtphost");
        if (null == smtpHostNode)
        {
            throw new CredException(this, "smtp host not found in " + credx);
        }
        smtpHost = smtpHostNode.InnerText;
    }
}
