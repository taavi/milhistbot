﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Wikimedia.MilHist
{	public class AClassMilestone : IMilestone
	{
		private string date;
		private string link;
		private int revId;
		private string result;

		public string Action
		{
			get => "ACR";
		}

		public string Date
		{
			get => date;
		}

		public string Link
		{
			get => link;
		}

		public int RevId
		{
			get => revId;
		}

		public string Result
		{
			get => result;
		}

		public override string ToString()
		{
			return ("date=" + Date + " link=" + Link + " oldid=" + RevId + " result=" + Result);
		}

		public AClassMilestone(AClassNominationPage nominationPage, string result)
		{
			date  = nominationPage.ArticlePage.TalkPage.Revision.Timestamp.ToString("HH:mm:ss dd MMMM yyyy (UTC)");
			link  = nominationPage.Title;
			revId = ArticleHistory.GetRevId(nominationPage.ArticlePage, date);
			this.result = result;
		}
	}

	public class ArchiveMainPage : Page
	{
		private readonly AClassNomination nomination;
		private readonly int year;

		private Section AssessmentSection(string status)
		{
			Link link = new Link(nomination.Path + year + "/" + status);
			Section section = new Section(this, "promoted");
			section.Add(link);
			return section;
		}

		internal void Archive()
		{
			try
			{
				Load();
			}
			catch (PageMissingException)
			{
				Template archive = new Template("WPMILHIST Archive");
				archive.Add("category", "review");
				Template navigation = new Template("WPMILHIST Navigation");
				Add(archive, Token.Newline, navigation, Token.Newline, new Token(" __FORCETOC__"), Token.Newline);

				Section promoted = AssessmentSection("Promoted");
				Section failed = AssessmentSection("Failed");
				Section kept = AssessmentSection("Kept");
				Section demoted = AssessmentSection("Demoted");
				Add(promoted, failed, kept, demoted);
			}

			Link category = new Link("Category:Requests for military history A-Class review|");
			Add(category, Token.Newline);
			Save("Create new archive page for " + year);
		}

		public ArchiveMainPage(AClassNomination nomination, AClassTalkPage talk) : base(nomination.Bot, nomination.Path + talk.Revision.Timestamp.Year)
		{
			this.nomination = nomination;
			year = talk.Revision.Timestamp.Year;
		}
	}

	public class ArchivePage : Page
	{
		private readonly AClassNomination nomination;
		private readonly string status;
		private readonly int year;

		public void Archive(NominationPage nomination)
		{
			try
			{
				Load();
			}
			catch (PageMissingException)
			{
				Section section = new Section(this, status);
				section.Add(
					new Comment("Please add new reviews directly below this line"),
					Token.Newline,
					Token.Newline,
					new Comment("Add archived reviews at the top please"),
					Token.Newline,
					Token.Newline,
					new Token("<noinclude>"),
					new Link("Category:" + status + " requests for military history A-Class review|"),
					new Token("</noinclude>"),
					Token.Newline
				);
                Add(section);
				Save("Created new page for " + year + "/" + status);
			}

			Comment? comment = FindComment("Please add new reviews directly below this line");
            if (comment == null)
            {
                throw new Exception("Could not find 'add new reviews' comment");
            }
            Insert(EndOfLine(comment), Token.Newline, nomination.Template);
        }

		public ArchivePage(AClassNomination nomination, AClassTalkPage talk, string status) : base(nomination.Bot, nomination.Path + talk.Revision.Timestamp.Year + "/" + status)
		{
			this.nomination = nomination;
			this.status = status;
			this.year = talk.Revision.Timestamp.Year;
		}
	}

	public class AClassArticlePage : ArticlePage
	{
		private readonly AClassNomination Nomination;
		private AClassTalkPage? talk;
		public AClassTalkPage TalkPage
		{
			get
			{
				if (null == talk)
				{
					talk = new AClassTalkPage(Nomination, "Talk:" + Title);
				}
				return talk;
			}
		}

		public AClassArticlePage(AClassNomination nomination, string title) : base(nomination, title)
		{
			Nomination = nomination;
		}
	}
	public class AClassTalkPage : TalkPage
	{
		private readonly AClassNomination Nomination;
		
		private string? status;
		private Revision revision;
		private bool revisionFound;

		internal Revision Revision
		{
			get
			{
				if (!revisionFound)
				{
					var history = History();
					foreach (var item in history)
					{
						AClassTalkPage itemPage = new AClassTalkPage(Nomination, Title, item.RevId);
						if (!itemPage.Status.Equals(Status))
						{
							revisionFound = true;
							break;
						}
						revision = item;
					}
				}
				if (!revisionFound)
				{
					throw new Exception("Could not find item in history");
				}
				return revision;
			}
		}

		public string Coordinator
		{
			get
			{
				return Revision.User;
			}
		}

		public string Status
		{
			get
			{
				if (null == status)
				{
					Load();
					status = MilHist.ProjectTemplate.Find("aclass")!.Value;
				}
				return status;
			}
		}
		
		public AClassTalkPage(AClassNomination nomination, string title, int revid = -1) : base(nomination, title, revid)
		{
			Nomination = nomination;
		}
	}

	public class AClassNominationPage : NominationPage
	{
		private readonly AClassNomination nomination;
		private AClassArticlePage? article;

        public AClassArticlePage ArticlePage
		{
			get
			{
				string? title;
				if (null == article)
				{
					Regex filter = new Regex(nomination.Regex);
					var match = filter.Match(Title);
					if (match.Success)
					{
						title = match.Groups["title"].Value;
					}
					else
					{
						throw new Exception("Bad nomination  name: " + Title);
					}
					article = new AClassArticlePage(nomination, title);
				}
				return article;
			}
		}

		public AClassNominationPage(AClassNomination nomination, Template template) : this(nomination, template, template.Name)
		{
		}

		public AClassNominationPage(AClassNomination nomination, Template template, string name) : base(nomination.Bot, nomination, template, name)
		{
			this.nomination = nomination;
		}
	}

	public class AClassNominationsPage : NominationsPage
	{
		private readonly AClassNomination nomination;
		private readonly List<AClassNominationPage> nominations;

		public new List<AClassNominationPage> Nominations
		{
			get
			{
				if (!nominations.Any())
				{
					Load();
					List<Template>? nominationTemplates = FindAll(nomination.Regex);
					foreach (Template template in nominationTemplates)
					{
						AClassNominationPage nominationPage = new AClassNominationPage(nomination, template);
						nominations.Add(nominationPage);
					}
				}
				return nominations;
			}
		}

		public void Remove(AClassNominationPage nomination)
		{
			Load();
			Remove(nomination.Template);
		}

		public AClassNominationsPage(AClassNomination nomination) : base(nomination)
		{
			this.nomination = nomination;
			nominations = new List<AClassNominationPage>();
		}
	}

	public class AClassNomination: Nomination
	{
		public new AClassNominationsPage NominationsPage
		{
			get
			{
				return new AClassNominationsPage(this);
			}
		}

		public AClassNomination(Bot bot) : base(bot)
		{
			Action = "ACR";
			Path = "Wikipedia:WikiProject Military history/Assessment/A-Class review";
			Regex = @"WikiProject Military history/Assessment/(?<title>(?!ACR/).+)";
		}
	}
}

