using System;
using System.Collections.Generic;
using NDesk.Options;

public class AutoCheck
{
	private Bot bot;

	private int unchanged;
	private int upgrade;
	private int newgrade;
	private int downgrade;

	private int  max     = 1;
	private bool all     = false;
	private bool debugs  = false;
	private bool force   = false;
	private bool help    = false;
	private bool update  = false;
	private bool verbose = false;
	private Dictionary<string, int> classes;

	private bool compareClass (string oldClass, string botClass)
	{
		if (null == classes)
		{
			classes = new Dictionary<string, int>()  
			{
				{ "Project",  0 },
				{ "Redirect", 0 },
				{ "Disambig", 0 },
				{ "Stub",     1 },
				{ "Start",    2 },
				{ "List",     2 },
				{ "C",        3 },
				{ "CL",       3 },
				{ "B",        4 },
				{ "BL",       4 },
			};
		}

		if (oldClass.Equals ("unclassified"))
		{
			newgrade++;
		}
		else if (! classes.ContainsKey (oldClass))
		{
			if (update)
			{
				newgrade++;
			}
			else
			{
				throw new ApplicationException ("unknown class: '" + oldClass + "'");
			}
		}
		else
		{	
			int diff = classes[botClass] - classes[oldClass];
			if (0 > diff)
			{
				downgrade++;
			}
			else if (0 == diff)
			{
				unchanged++;
				return false;
			}
			else if (0 < diff)
			{
				upgrade++;
			}
		}
		return true;
	}

	private void updateAllProjects (Page talk)
	{
		var milhist  = talk.MilHist.ProjectTemplate;
		var projects = talk.FindAll ("^WikiProject ");
		foreach (var project in projects)
		{
			if (! MilHist.IsMilHist (project))
			{
				project.Add ("class", milhist.Class);
				if (project.Name.Matches ("Aviation|Germany|Ships"))
				{
					project.Add ("b1", milhist.HasReferences);
					project.Add ("b2", milhist.HasCoverage);
					project.Add ("b3", milhist.HasStructure);
					project.Add ("b4", milhist.HasGrammar);
					project.Add ("b5", milhist.HasSupport);	
				}	
			}	
		}
	}

	private void autoCheck (Page article, Page talk)
	{       
		try
		{         
			bot.Cred.Showtime (article.Title);
			article.Load ();
			try
			{
				talk.Load ();
			}
			catch (PageMissingException)
			{
				if (force || update)
				{
					bot.Cred.Showtime (talk.Title + " does not exist - creating it");
				}
			}
			
			var template = talk.MilHist.ProjectTemplate;
			var oldClass = template.Class;
			var missing  = template.Missing ();
			var rating   = template.Rating;
			var botClass = rating.Class;
			
			if (debugs || verbose)
			{
				Console.WriteLine  ("\tOriginal: " + template.Text);
			}
			
			if (update)
			{
				template.RemoveAll ("^class$|^importance$|^b\\d$|^B-Class-\\d$");
			}

			template.Rating = rating;
			if (all)
			{
				updateAllProjects (talk);
			}
			
			if (debugs || verbose)
			{			
				Console.WriteLine ("\told rating = " + oldClass);
				Console.WriteLine ("\tprediction = " + article.Prediction ()); 
				Console.WriteLine ("\tbot rating = " + botClass);				
				Console.WriteLine ("\tModified: " + template.Text);
			}
			
			var changed = compareClass (oldClass, botClass);
			if (((changed || missing) && force) || update)
			{
				talk.Save ("Automatic MILHIST checklist assessment - " + botClass + " class");
				if (changed)
				{
					bot.Cred.Showtime ("\tChanged from " + oldClass + " to " + botClass + " class");
				}
				else if (missing)
				{
					bot.Cred.Showtime ("\tAdded missing checklist items");               		
				}
				else
				{
					bot.Cred.Showtime ("\tUpdated to " + botClass + " class");              	    
				}
			}
		}
		catch (ApplicationException ex) 
		{
			string message = "Error in " + article.Title + ": " + ex.Message;
			bot.Cred.Showtime (message);
		}
		catch (Exception ex)
		{
			string message = "Error in " + article.Title + ":\n" + ex.Message + "\n" + ex.StackTrace;
			if (debugs)
			{
				Debug.WriteLine (message);
			}
			else
			{
				bot.Cred.Warning (message);
				bot.Close ();
			}
			Environment.Exit (1);    
		}
	}
	
	private void autoCheck (List<Page> talkPages)
	{
		foreach (var talkPage in talkPages) 
		{   
			// Could be a subcategory
			if (talkPage.Namespace.Equals ("Talk"))
			{
				autoCheck (talkPage.Article, talkPage);
			}
		}   
		bot.Cred.Showtime (String.Format("{0} articles newly rated, {1} downgraded, {2} upgraded, {3} unchanged - total {4}",
							newgrade, downgrade, upgrade, unchanged, newgrade + downgrade + upgrade + unchanged));   
	}

	private static void showHelp (OptionSet options)
	{
		Console.WriteLine ("Usage: mono AutoCheck [OPTIONS]+ <article>");
		Console.WriteLine ("Assess article and update the MilHist template on the talk page.");
		Console.WriteLine ();
		Console.WriteLine ("Options:");
		options.WriteOptionDescriptions (Console.Out);
		Environment.Exit (0);
	}

	List<string> options (string [] args)
	{
		var optionSet = new OptionSet () {
			{ "a|all",       "all projects", v => all     = v != null },
			{ "d|debug",     "debugging",    v => debugs  = v != null },
			{ "f|force",     "update page",  v => force   = v != null },
			{ "ff|u|update", "force update page",  v => update = v != null },
			{ "h|?|help",    "display help", v => help    = v != null },
			{ "n|max=",      "number of pages to process",  v => int.TryParse (v, out max) },
			{ "v|verbose",   "vebosity",     v => verbose = v != null },
		};

		List<string> extras = optionSet.Parse (args);

		if (help)
		{
			showHelp (optionSet);
		}

		return extras;
	}

	private AutoCheck (string [] args)
	{
		bot = new Bot ();
		var articles = options (args);
		Debug.On = debugs;
		Debug.Bot = bot;
		List<Page> talkPages;

		bot.Cred.Showtime ("started");
		if (articles.Count > 0)
		{
			var articlePage = new Page (bot, articles[0]);
			autoCheck (articlePage, articlePage.Talk);
		}
		else
		{
			var query = new Query ("Military history articles with missing B-Class checklists", max);
			talkPages = bot.Category (query);
		
			if (talkPages.Count < max)
			{
				query = new Query ("Military history articles with incomplete B-Class checklists", max - talkPages.Count);
				talkPages.AddRange (bot.Category (query));
			}

			if (talkPages.Count < max)
			{
				query = new Query ("Unassessed military history articles", max - talkPages.Count);
				talkPages.AddRange (bot.Category (query));
			}
			autoCheck (talkPages);
		}           
		bot.Cred.Showtime ("done");
	}

	static public void Main (string [] args)
	{
		new AutoCheck (args);
	}
}
