using System;
using System.Collections.Generic;
using NDesk.Options;

public class AutoRef
{
	private Bot  bot;
	private bool debugs  = false;
	private bool force   = false;
	private bool help    = false;
	private Dictionary<string, Template> lookup;

	private static void showHelp (OptionSet options)
	{
		Console.WriteLine ("Usage: mono Autoref [OPTIONS]+ <article>");
		Console.WriteLine ("Reformat the references to match the command line.");
		Console.WriteLine ();
		Console.WriteLine ("Options:");
		options.WriteOptionDescriptions (Console.Out);
		Environment.Exit (0);
	}

	List<string> options (string [] args)
	{
		var optionSet = new OptionSet () {
			{ "d|debug",     "debugging",    v => debugs  = v != null },
			{ "f|force",     "update page",  v => force   = v != null },
			{ "h|?|help",    "display help", v => help    = v != null },
		};

		List<string> extras = optionSet.Parse (args);

		if (help)
		{
			showHelp (optionSet);
		}

		return extras;
	}
	
	private void updateRef (Reference reference, Tokens element, Int32 index)
	{
		Console.WriteLine ("Found reference: " + reference);
		if (reference.Contents.Contents[0] is Template)
		{
			Template template = reference.Contents.Contents[0] as Template;
			if (template.Name.Equals ("harvnb", StringComparison.OrdinalIgnoreCase))
			{
				template.Name = "sfn";
				element.Contents[index] = template;
				if (null != reference.Name)
				{
					lookup[reference.Name] = template;					
				}
			}
		}	
	}
	
	private void autoRef (string title)
	{
		Debug.Entered ("title=" + title);
		Page page = new Page (bot, title);
		page.Load ();
		
		lookup = new Dictionary<string, Template>();
		for (int pass = 0; pass < 2; pass++)
		{
			Debug.WriteLine ("pass " + pass);
			foreach (var reference in page.References)
			{
				Tokens element = Tree.Find (page.Contents, reference);
				Int32 index = element.Contents.IndexOf (reference);
				
				if (null == reference.Name)
				{
					updateRef (reference, element, index);
				}	
				else if (null == reference.Contents)
				{
					Console.WriteLine ("Found named reference instance: " + reference);
					if (lookup.ContainsKey(reference.Name))
					{
						element.Contents[index] = lookup[reference.Name];	
					}	
				}
				else
				{
					updateRef (reference, element, index);
				}
			}
		}
		page.Save ("Converted harvnb templates to sfn templates");
		Debug.Exited ();
	}

	private AutoRef (string [] args)
	{
		bot = new Bot ();
		var titles = options (args);
		Debug.On = debugs;
		Debug.Bot = bot;

		bot.Cred.Showtime ("started");
		foreach (var title in titles) {
			autoRef (title);	
		}
		bot.Cred.Showtime ("done");
	}

	static public void Main (string [] args)
	{
		new AutoRef (args);
	}
}
