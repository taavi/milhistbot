﻿using System;

namespace Wikimedia.MilHist
{
	public class AnnouncementsTemplate : Template
	{
		public new const string Name = "WPMILHIST Announcements/Shell";

		private static Comment BotGenerated => new Comment("Bot generated");
		public static bool IsAnnouncements(string name)
		{
			return Name.Equals(name);
		}

		private static string FormatList(List<string> strings)
        {
			return String.Join(" &bull; ", strings) + BotGenerated + "\n" + "\n";
		}

		private static string FormatList(List<AClassNominationPage> nominations)
		{
			List<string> strings = new List<string>();
			foreach (var nomination in nominations)
			{
				var link = new Link(nomination.Title, nomination.ArticlePage.Title);
				strings.Add(link.Text);
			}
			return FormatList(strings);
		}

		private static string FormatList(List<NominationPage> nominations)
		{
			List<string> strings = new List<string>();
			foreach (var nomination in nominations)
			{
				var link = new Link(nomination.Title, nomination.Article.Title);
				strings.Add(link.Text);
			}
			return FormatList(strings);
		}

		private static string FormatList(List<GANominationPage> nominations)
		{
			List<string> strings = new List<string>();
			foreach (var nomination in nominations)
			{
				Template template = new Template("WPMHA/GAN");
                template["1"] = nomination.Template["1"];
				template["2"] = nomination.Template["2"];
				strings.Add(template.Text);
			}
			return FormatList(strings);
		}

		public void RebuildFeatured()
		{
			Featured.Featured featured = new Featured.Featured(Page.Bot);
			var nominations = featured.NominationsPage.Nominations.FindAll(item => MilHist.IsMilHist(item.Article.Talk));
			this["featured_article_candidates"] = FormatList(nominations);
		}

		public void RebuildFeaturedReview()
		{
			Featured.Review review = new Featured.Review(Page.Bot);
			var nominations = review.NominationsPage.Nominations.FindAll(item => MilHist.IsMilHist(item.Article));
			this["featured_article_reviews"] = FormatList(nominations);
		}

		public void RebuildFeaturedList()
		{
			Featured.List list = new Featured.List(Page.Bot);
			var nominations = list.NominationsPage.Nominations.FindAll(item => MilHist.IsMilHist(item.Article));
			this["featured_list_candidates"] = FormatList(nominations);
		}

		public void RebuildAClass()
		{
			AClassNomination AClass = new AClassNomination(Page.Bot);
			var nominations = AClass.NominationsPage.Nominations;
			this["A-Class_reviews"] = FormatList(nominations);
		}

		public void RebuildPeerReview()
		{
			PeerReview PeerReviewNomination = new PeerReview(Page.Bot);
			var nominations = PeerReviewNomination.NominationsPage.Nominations.FindAll(item => MilHist.IsMilHist(item.Article));
			this["peer_reviews"] = FormatList(nominations);
		}

		public void RebuildGoodArticles()
		{
			GANomination GoodArticleNomination = new GANomination(Page.Bot);
			var nominations = GoodArticleNomination.GANominationsPage.GANominations.FindAll(item => MilHist.IsMilHist(item.Article));
			this["good_article_nominees"] = FormatList(nominations);
		}

		public void RebuildGoodArticleReassessments()
		{
			GAReassessment GAReassessment = new GAReassessment(Page.Bot);
			var nominations = GAReassessment.GAReassessmentsPage.GAReassessments.FindAll(item => MilHist.IsMilHist(item.Article));
			this["good_article_reasessments"] = FormatList(nominations);
		}

		public void Rebuild()
		{
			RebuildFeatured();
			RebuildFeaturedReview();
			RebuildFeaturedList();
			RebuildAClass();
			RebuildPeerReview();
			RebuildGoodArticles();
			RebuildGoodArticleReassessments();
		}

		public AnnouncementsTemplate(Page page, Tokens tokens) : base(page, tokens)
		{
		}
	}

	public class AnnouncementsPage : Page
	{
		new const string Title = "Template:WPMILHIST Announcements";

		private AnnouncementsTemplate? announcementsTemplate;

		public AnnouncementsTemplate AnnouncementsTemplate
		{
			get
			{
				if (null == announcementsTemplate)
				{
					Load();
                    announcementsTemplate = Find(AnnouncementsTemplate.Name) as AnnouncementsTemplate;
					if (null == announcementsTemplate)
					{
						throw new Exception("Cannot find announcements template");
					}
				}
				return announcementsTemplate;
			}
		}

		public void Rebuild()
		{
			AnnouncementsTemplate.Rebuild();
		}

		public AnnouncementsPage(Bot bot, int revid = -1) : base(bot, Title, revid)
		{
		}
	}
}
