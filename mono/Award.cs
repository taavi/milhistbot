using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;


namespace Wikimedia.MilHist
{
    public class Award
    {
        public string Name { get; set; }
        public string Nominee { get; set; }
        public string Nominator { get; set; }
        public string Citation { get; set; }
        public int Diff { get; set; }
        public int OldId { get; set; }

        private readonly Bot bot;
        private string? approver;
        private AwardNominationTemplate? nomination;
        private AwardNominationsPage? nominations;
        private TrackingPage? tracking;
        private static readonly Dictionary<string, string> templateNames;

        public string Approver
        {
            get
            {
                if (null == approver)
                {
                    var history = Nominations.History();
                    foreach (var revision in history)
                    {
                        var page = new AwardNominationsPage(bot, this, revision.RevId);
                        page.Load();
                        var nominations = page.Templates.FindAll(x => x is AwardNominationTemplate);
                        AwardNominationTemplate? nom = nominations.Find(x => ((AwardNominationTemplate)x).Matches(Nomination)) as AwardNominationTemplate;
                        if (null == nom)
                        {
                            throw new Exception("Could not find award nomination template");
                        }
                        else if (null == nom.Status)
                        {
                            throw new Exception("Could not find Status parameter in award nomination template");
                        }
                        else if (nom.Status.Equals("approved"))
                        {
                            approver = revision.User;
                        }
                        else if (nom.Status.Equals("nominated"))
                        {
                            break;
                        }
                    }
                }
                if (null == approver)
                {
                    throw new Exception("Could not find award nomination approver");
                }
                return approver;
            }
            set => approver = value;
        }

        public int Maximum
        {
            get => Name.Matches("A-Class cross") ? 5 : 3;
        }

        public AwardNominationTemplate Nomination
        {
            get
            {
                if (null == nomination)
                {
                    nomination = new AwardNominationTemplate(Name, Nominee, Citation);
                }
                return nomination;
            }
            set => nomination = value;
        }

        public AwardNominationsPage Nominations
        {
            get
            {
                if (null == nominations)
                {
                    nominations = new AwardNominationsPage(bot, this);
                }
                return nominations;
            }
        }

        public TrackingPage TrackingPage
        {
            get
            {
                if (null == tracking)
                {
                    tracking = new TrackingPage(bot, this);
                }
                return tracking;
            }
            set => tracking = value;
        }

        private static string recommendation(Bot bot, string nominee)
        {
            string name;

            var count = AwardNominationsPage.Count(bot, nominee);

            if (count < 35)
            {
                if (count < 5)
                {
                    name = "A-Class medal";
                }
                else if (count < 10)
                {
                    name = "A-Class medal with Oak Leaves";
                }
                else if (count < 20)
                {
                    name = "A-Class medal with Swords";
                }
                else
                {
                    name = "A-Class medal with Diamonds";
                }

            }
            else
            {
                if (count < 40)
                {
                    name = "A-Class cross";
                }
                else if (count < 46)
                {
                    name = "A-Class cross with Oak Leaves";
                }
                else if (count < 56)
                {
                    name = "A-Class cross with Swords";
                }
                else
                {
                    name = "A-Class cross with Diamonds";
                }
            }
            return name;
        }

        public void Nominate()
        {
            if (null != Nominator)
            {
                Nominations.Add(Nominator, Nomination);
            }
        }

        public void Congratulate()
        {
            Page page = new Page(bot, "User talk:" + Nominee);
            string subject = "Congratulations from the Military History Project";

            Template approverTemplate = new Template("user0");
            approverTemplate.Add(1, Approver);

            var awardTemplateName = templateNames[Name];
            Template awardTemplate = new Template("subst:" + awardTemplateName);
            awardTemplate.Add(1, "On behalf of the Military History Project, I am proud to present the " + Name + " for " + Citation + " " + approverTemplate + " via ~~~~");

            Section section = new Section(page, subject, 2);
            section.Add("\n");
            section.Add(awardTemplate);
            section.Add("\n");
            page.Save(section, subject);

            Nomination.Status = "awarded";
            Nomination.Diff = page.RevId.ToString();
            Nomination.OldId = page.OldRevId.ToString();
            Nomination.Save("Awarded " + Name + " to " + Nominee);

            Diff = page.RevId;
            OldId = page.OldRevId;
        }

 
        static Award()
        {
            templateNames = new Dictionary<string, string>()
            {
                {"A-Class medal",                           "WPMILHIST A-Class medal"                 },
                {"A-Class medal with Oak Leaves",           "WPMILHIST A-Class medal (Oakleaves)"     },
                {"A-Class medal with Swords",               "WPMILHIST A-Class medal (Swords)"        },
                {"A-Class medal with Diamonds",             "WPMILHIST A-Class medal (Diamonds)"      },

                {"A-Class cross",                           "WPMILHIST A-Class cross"                 },
                {"A-Class cross with Oak Leaves",           "WPMILHIST A-Class cross with Oak Leaves" },
                {"A-Class cross with Swords",               "WPMILHIST A-Class cross with Swords"     },
                {"A-Class cross with Diamonds",             "WPMILHIST A-Class cross with Diamonds"   },

                {"WikiChevrons",                            "WPMILHIST WikiChevrons"                  },
                {"WikiChevrons with Oak Leaves" ,           "WPMILHIST WikiChevrons with Oak Leaves"  },
                {"The Milhist reviewing award (1 stripe)",  "WPMILHIST Service (review) 1 stripe"     },
                {"The Milhist reviewing award (2 stripes)", "WPMILHIST Service (review) 2 stripes"    },
                {"The Milhist reviewing award (3 stripes)", "WPMILHIST Service (review) 3 stripes"    },
                {"The Content Review Medal of Merit (Military history)", "WPMILHIST Service (review) Content Review Medal of Merit" },
            };
        }

        public Award(Bot bot, string name, string nominee = "", string citation = "")
        {
            this.bot = bot;
            Name = name;
            Nominee = nominee;
            Citation = citation;
            Nominator = "";
        }

        public Award(Bot bot, string nominee, string citation) : this(bot, recommendation(bot, nominee), nominee, citation)
        {
        }

        public Award(Bot bot, string nominator, AwardNominationTemplate nomination) : this(bot, nomination.Award, nomination.Nominee , nomination.Citation)
        {
            this.nomination = nomination;
            this.Nominator = nominator;
        }
    }

    public class AwardNominationTemplate : Template
    {
        private new const string Name = "WPMILHIST Award nomination";

        public string Award { get => this["award"] ?? ""; set => this["award"] = value; }
        public string Nominee { get => this["nominee"] ?? ""; set => this["nominee"] = value; }
        public string Citation { get => this["citation"] ?? ""; set => this["citation"] = value; }
        public string Status { get => this["status"] ?? ""; set => this["status"] = value; }
        public string Diff { get => this["diff"] ?? ""; set => this["diff"] = value; }
        public string OldId { get => this["oldid"] ?? ""; set => this["oldid"] = value; }

        public static bool IsNomination(string name)
        {
            return Name.Equals(name);
        }

        public bool Matches(AwardNominationTemplate nomination)
        {
            if (null == nomination.Award || null == nomination.Nominee || null == nomination.Citation)
            {
                return false;
            }
            return nomination.Award.Equals(Award) && nomination.Nominee.Equals(Nominee) && nomination.Citation.Equals(Citation);
        }

        public void Save(string summary)
        {
            Page.Save(summary);
        }

        public AwardNominationTemplate(string award, string nominee, string citation, string status = "nominated") : base(Name)
        {
            Award = award;
            Nominee = nominee;
            Citation = citation;
            Status = status;
        }

        public AwardNominationTemplate(Page page, Tokens tokens) : base(page, tokens)
        {
        }
    }

    // Deals with the nominations page
    public class AwardNominationsPage : Page
    {
        private const string acmSectionName = "Nominations for the A-Class Medal";
        private const string accSectionName = "Nominations for the A-Class Cross";
        private const string nominationPageTitle = "Wikipedia talk:WikiProject Military history/Awards";

        private readonly Award award;
        private readonly Bot bot;

        public static int Count(Bot bot, string nominee)
        {
            List<string> archives = new List<string>() {
            "Wikipedia talk:WikiProject Military history/Awards",
            "Wikipedia talk:WikiProject Military history/Awards/ACC/Archive 1",
            "Wikipedia talk:WikiProject Military history/Awards/ACR/Archive 2",
            "Wikipedia talk:WikiProject Military history/Awards/ACR/Archive 1",
        };

            var count = 0;
            foreach (var archive in archives)
            {
                Page page = new Page(bot, archive);
                page.Load();
                var section = page.FindSection(nominee + @" \((\d+)\)");
                if (null != section)
                {
                    Regex regex = new Regex(@" \((\d+)\)", RegexOptions.IgnoreCase);
                    Match match = regex.Match(section.Title);
                    Group group = match.Groups[1];
                    count = Int32.Parse(group.ToString());
                    break;
                }
            }
            return count;
        }

        public void Add(string nominator, AwardNominationTemplate nomination)
        {
            var section = new Section(this, SectionName(award));
            var count = Count(bot, award.Nominee) + 1;
            string subject = award.Nominee + " (" + count + ")";
            string text = "\n" + nomination.Text + "\n" + "*'''Support''' As nominator " + nominator + " via ~~~~" + "\n";

            Section subsection = new Section(this, subject, 4);
            subsection.Add("\n");
            subsection.Add(text);
            subsection.Add("\n");
            section.Add(subsection);
            section.Save(subject);
        }

        public IEnumerable<AwardNominationTemplate> Approved()
        {
            foreach (var template in Templates)
            {
                if (template is AwardNominationTemplate)
                {
                    var nomination = template as AwardNominationTemplate;
                    if (nomination != null && nomination.Status != null)
                    {
                        if (nomination.Status.Equals("approved"))
                        {
                            yield return nomination;
                        }
                    }
                }
            }
        }

        public static string SectionName(Award award)
        {
            string nominationSectionName;
            if (award.Name.Matches("A-Class medal"))
            {
                nominationSectionName = acmSectionName;
            }
            else if (award.Name.Matches("A-Class cross"))
            {
                nominationSectionName = accSectionName;
            }
            else
            {
                throw new Exception("unknown award: " + award);
            }
            return nominationSectionName;
        }

        public AwardNominationsPage(Bot bot, Award award, int revid = -1) : base(bot, nominationPageTitle, revid)
        {
            this.bot = bot;
            this.award = award;
        }

    }

    // Deals with the tracking page
    public class TrackingPage : Page
    {
        private const string nominationPageTitle = "Wikipedia:WikiProject Military history/Awards";
        private const string acmTracking = nominationPageTitle + "/ACM/Eligibility tracking";
        private const string accTracking = nominationPageTitle + "/ACC/Eligibility tracking";

        private readonly Award award;
        private List<Link>? tally;

        public void Increment(string entry)
        {
            var image = new ExtendedImage("Symbol a class.svg");
            image.Size = "15px";
            image.ImageLink = "link=" + entry;
            Insert(Tally.Last(), new Token(" "), image);
        }

        public IEnumerable<string> Entries()
        {
            foreach (var link in Tally)
            {
                yield return link.Value.Substring(10);
            }
        }

        public void Erase()
        {
            foreach (var link in Tally)
            {
                var index = FindIndex(link);
                RemoveRange(index, 1);
            }
        }

        public bool IsComplete()
        {
            return Tally.Count + 1 >= award.Maximum;
        }

        public List<Link> Tally
        {
            get
            {
                if (null == tally)
                {
                    tally = new List<Link>();
                    IToken? text = FindText(award.Nominee);
                    if (null == text)
                    {
                        throw new Exception("unable to find award nominee text");
                    }
                    int? index = FindIndex(text);
                    if (null == index)
                    {
                        throw new Exception("unable to find award nominee index"); 
                    }
                    for (var i = (int)index + 1; ; ++i)
                    {
                        IToken? token = GetToken(i);
                        if (null != token && token is Link)
                        {
                            tally.Add((Link)token);
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                return tally;
            }
            set
            {
                tally = value;
            }
        }

        public static string TrackingPageTitle(string name)
        {
            string trackingPageTitle;
            if (name.Matches("A-Class medal"))
            {
                trackingPageTitle = acmTracking;
            }
            else if (name.Matches("A-Class cross"))
            {
                trackingPageTitle = accTracking;
            }
            else
            {
                throw new Exception("unknown page: " + name);
            }
            return trackingPageTitle;
        }

        public TrackingPage(Bot bot, Award award) : base(bot, TrackingPageTitle(award.Name))
        {
            this.award = award;
        }
    }

    public class Historical : Page
    {
        private static Dictionary<string, string> awardPageTitles;

        private Tokens entry(Award award)
        {
            Tokens tokens = new Tokens();

            Link user = new Link(new Namespace("User"), award.Nominee, award.Nominee);

            Template diff = new Template("diff");
            diff.Add("page", "User talk:" + award.Nominee);
            diff.Add("diff", award.Diff.ToString());
            diff.Add("oldid", award.OldId.ToString());
            DateTime currentDate = DateTime.Now;
            diff.Add("label", currentDate.ToString("MMMM YYYY"));

            tokens.Add("* ");
            tokens.Add(user);
            tokens.Add(": for ");
            tokens.Add(award.Citation);
            tokens.Add(" (Awarded ");
            tokens.Add(diff);
            tokens.Add(")\n");

            return tokens;
        }

        public void Add(Award award)
        {
            if (award.Name.Equals("WikiChevrons with Oak Leaves"))
            {
                var link = Links.Find(x => x.Namespace.Equals("User") && String.Compare(x.Data, award.Nominee) > 0);
                if (link == null)
                {
                    Add(Links[^1], entry(award));
                }
                else
                {
                    Insert(StartOfLine(link as IToken), entry(award));
                }
            }
            else
            {
                Section? section = Sections.Find(ind => ind.Title.Equals(award.Name));
                if (null == section)
                {
                    section = new Section(this, award.Name, 4);
                    section.Add("The ''" + award.Name + "'' was introduced in May 2014 and has been awarded to the following editors:\n");
                    section.Add(entry(award));
                }
                else
                {
                    int first = FindIndex(section);
                    int index = Sections.IndexOf(section);
                    int last = (index < Sections.Count - 1) ? FindIndex(Sections[index + 1]) : FindLastIndex();
                    var links = Links.FindAll(x => x.Namespace.Equals("User") && FindIndex(x) > first && FindIndex(x) <= last);
                    var link = links.Find(x => String.Compare(x.Data, award.Nominee) > 0);
                    if (link == null)
                    {
                         Add(links[^1], entry(award));
                    }
                    else
                    {
                        Insert(StartOfLine(link as IToken), entry(award));
                    }
                }
            }
        }

        static Historical()
        {
            const string nominationPagePath = "Wikipedia:WikiProject Military history/Awards/";
            awardPageTitles = new Dictionary<string, string>()
        {
            {"WikiChevrons with Oak Leaves",  nominationPagePath + "OAK" },

            {"A-Class medal",                 nominationPagePath + "ACM" },
            {"A-Class medal with Oak Leaves", nominationPagePath + "ACM" },
            {"A-Class medal with Swords",     nominationPagePath + "ACM" },
            {"A-Class medal with Diamonds",   nominationPagePath + "ACM" },

            {"A-Class cross",                 nominationPagePath + "ACC" },
            {"A-Class cross with Oak Leaves", nominationPagePath + "ACC" },
            {"A-Class cross with Swords",     nominationPagePath + "ACC" },
            {"A-Class cross with Diamonds",   nominationPagePath + "ACC" },

        };
        }

        public Historical(Bot bot, Award award) : base(bot, awardPageTitles[award.Name])
        {
        }
    }
}
