﻿using System;

namespace Wikimedia
{
    public class TemplateFactory
    {
        public Template GetTemplate(Page page, Tokens tokens)
        {
            string name = tokens.Contents[0].Text;

            if (MilHist.AnnouncementsTemplate.IsAnnouncements(name))
            {
                return new MilHist.AnnouncementsTemplate(page, tokens);
            }
            else if (ArticleHistory.IsArticleHistory(name))
            {
                return new ArticleHistory(page, tokens);
            }
            else if (ArticleForDeletion.IsArticleForDeletion(name))
            {
                return new ArticleForDeletion(page, tokens);
            }
            else if (DidYouKnow.IsDidYouKnow(name))
            {
                return new DidYouKnow(page, tokens);
            }
            else if (GoodArticles.IsGoodArticle(name))
            {
                return new GoodArticles(page, tokens);
            }
            else if (InTheNews.IsInTheNews(name))
            {
                return new InTheNews(page, tokens);
            }
            else if (OldPeerReview.IsOldPeerReview(name))
            {
                return new OldPeerReview(page, tokens);
            }
            else if (OnThisDay.IsOnThisDay(name))
            {
                return new OnThisDay(page, tokens);
            }
            else if (MilHist.AwardNominationTemplate.IsNomination(name))
            {
                return new MilHist.AwardNominationTemplate(page, tokens);
            }
            else if (MilHist.MilHist.IsMilHist(name))
            {
                return new MilHist.ProjectTemplate(page, tokens);
            }
            else
            {
                return new Template(page, tokens);
            }
        }

        public TemplateFactory()
        {
        }
    }
}
