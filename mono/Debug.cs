using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Wikimedia
{
    public class Debug
    {
        public static bool On { get; set; }
        public static Bot? Bot { get; set; }
        public static Dictionary<string, bool> Module = new Dictionary<string, bool>();

        public static string Caller(Int32 skipFrames = 2)
        {
            var stackTrace = new StackTrace(new StackFrame(skipFrames));
            return stackTrace.GetFrame(0)!.GetMethod()!.Name;
        }

        public static void Entered(string message = "", string name = "")
        {
            WriteLine(Caller() + " entered " + message, name);
        }

        public static void Exited(string message = "", string name = "")
        {
            WriteLine(Caller() + " exited " + message, name);
        }

        public static void WriteLine(string message, string name = "")
        {
            if (On || (Module.ContainsKey(name) && Module[name]))
            {
                if (null == Bot)
                {
                    Console.Error.WriteLine(message);
                }
                else
                {
                    Bot.Cred.Showtime(message);
                }
            }
        }
    }
}