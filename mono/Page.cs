using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using Wikimedia.MilHist;

namespace Wikimedia
{
    public interface IPageElement
    {
        void Added(Page page);
        void Removed(Page page);
    }

    public class ParseException : Exception
    {
        public ParseException(string message) : base(message)
        {
        }
    }

    public class Page
    {
        public Bot Bot { get; set; }
        public int Ns { get; set; }
        public int PageId { get; set; }
        public bool ReadOnly { get; set; }
        public int RevId { get; set; }
        public int OldRevId { get; set; }
        public List<Link> Links { get; set; }
        public List<Reference> References { get; set; }
        public List<Section> Sections { get; set; }
        public List<Template> Templates { get; set; }
        public ArticleHistory ArticleHistory { get; set; }
        public Query? Query { get; set; }
        public MilHist.MilHist MilHist { get; set; }

        private List<string>? categories;
        private Namespace? _namespace;
        private Page? _talk;
        private Page? _article;
        private Tokens _tokens;
        private string? prediction;
        private bool checkedRedirect;
        private Page? redirectsTo;
        private bool loaded;

        public string Content
        {
            get => _tokens.Text;
            set => Parse(value);
        }

        public Tokens Contents
        {
            get => _tokens;
        }

        public Page Article
        {
            get
            {
                if (null == _article)
                {
                    Debug.Entered("setArticle: title='" + Title + "'", "Page::setArticle");
                    if (Namespace.IsTalkspace)
                    {
                        var articleTitle = Namespace.Text.Equals("Talk") ? TitleWithoutNamespace : Namespace.Mainspace + ":" + TitleWithoutNamespace;
                        _article = new Page(Bot, articleTitle);
                    }
                    else
                    {
                        _article = this;
                    }
                    Debug.Exited("setArticle: article='" + _article.Title + "'", "Page::setArticle");
                }
                return _article;
            }
            set => _article = value;
        }

        public bool IsDisambiguation
        {
            get
            {
                return InCategory("disambiguation") && !InCategory("links needing disambiguation");
            }
        }

        public bool IsProject
        {
            get
            {
                return Namespace.Equals("Wikipedia");
            }
        }

        public bool IsRedirect
        {
            get
            {
                return null != RedirectsTo;
            }
        }

        public Namespace Namespace
        {
            get
            {
                if (null == _namespace)
                {
                    if (null != Title)
                    {
                        Match match = Regex.Match(Title, @"^(?<namespace>.+?):");
                        if (match.Success)
                        {
                            string s = match.Groups["namespace"].Value;
                            if (s != null && Wikimedia.Namespace.IsNamespace(s))
                            {
                                _namespace = new Namespace(s);
                            }
                        }
                    }
                }
                if (null == _namespace)
                {
                    _namespace = new Namespace("");
                }
                return _namespace;
            }
            set => _namespace = value;
        }

        public Page? RedirectsTo
        {
            get
            {
                if (!checkedRedirect)
                {
                    redirectsTo = Bot.RedirectsTo(Title);
                    checkedRedirect = true;
                }
                return redirectsTo;
            }
        }

        public Page Talk
        {
            get
            {
                if (null == _talk)
                {
                    Debug.Entered("setTalk: title='" + Title + "'", "Page::setTalk");
                    if (Namespace.IsTalkspace)
                    {
                        _talk = this;
                    }
                    else
                    {
                        Debug.WriteLine("Namespace=" + Namespace, "Page::setTalk");
                        var talkTitle = Namespace.Talkspace + ":" + TitleWithoutNamespace;
                        _talk = new Page(Bot, talkTitle);
                    }
                    Debug.Exited("setTalk: talk='" + _talk.Title + "'", "Page::setArticleAndTalk");
                }
                return _talk;
            }
            set => _talk = value;
        }

        public string Title { get; set; }
 
        public string TitleWithoutNamespace
        {
            get => Namespace.Text.Equals("") ? Title : Title.Substring(Namespace.Text.Length + 1);
        }

        private void SetArticleAndTalk()
        {
            Debug.Entered("setArticleAndTalk: title='" + Title + "'", "Page::setArticleAndTalk");
            if (Namespace.IsTalkspace)
            {
                _talk = this;       
                var articleTitle = Namespace.Text.Equals("Talk") ? TitleWithoutNamespace : Namespace.Mainspace + ":" + TitleWithoutNamespace;
                _article = new Page(Bot, articleTitle);
                _article.Article = _article;
                _article.Talk = _talk;
            }
            else
            {
                Debug.WriteLine("Namespace=" + Namespace, "Page::setArticleAndTalk");
                _article = this;
                var talkTitle = Namespace.Talkspace + ":" + TitleWithoutNamespace;
                _talk = new Page(Bot, talkTitle);
                _talk.Article = _article;
                _talk.Talk = _talk;
            }
            Debug.Exited("setArticleAndTalk: article='" + _article.Title + "' talk='" + _talk.Title + "'", "Page::setArticleAndTalk");
        }


        public override string ToString()
        {
            return Content;
        }

        private void Parse(string content)
        {
            _tokens = new Tokens();
            var templateFactory = new TemplateFactory();
            Tokens tokens = new Tokens(content);
            while (!tokens.Empty())
            {
                var token = tokens.Dequeue();
                try
                {
                    switch (token.Type)
                    {
                        case TokenType.OpenComment:
                            _tokens.Add(new Comment(this, tokens));
                            break;
                        case TokenType.OpenNowiki:
                            _tokens.Add(new Nowiki(this, tokens));
                            break;
                        case TokenType.OpenBracket:
                            _tokens.Add(new Link(this, tokens));
                            break;
                        case TokenType.OpenExternalLink:
                            _tokens.Add(new ExternalLink(this, tokens));
                            break;
                        case TokenType.OpenTemplate:
                            _tokens.Add(templateFactory.GetTemplate(this, tokens));
                            break;
                        case TokenType.OpenBlockquote:
                            _tokens.Add(new Blockquote(this, tokens));
                            break;
                        case TokenType.Reference:
                            _tokens.Add(new Reference(this, token));
                            break;
                        case TokenType.OpenReference:
                            _tokens.Add(new Reference(this, token, tokens));
                            break;
                        case TokenType.OpenTable:
                            _tokens.Add(new Table(this, tokens));
                            break;
                        case TokenType.OpenGallery:
                            _tokens.Add(new Gallery(this, token, tokens));
                            break;
                        case TokenType.MultiEquals:
                            _tokens.Add(new Section(this, tokens));
                            break;
                        default:
                            _tokens.Add(token);
                            break;
                    }
                }
                catch (ParseException)
                {
                    _tokens.Add(token);
                }
            }
        }

        public void Add(params IToken[] items)
        {
            _tokens.Contents.AddRange(items);
            Added(items);
        }

        public void Add(string s)
        {
            Add(new Tokens(s));
        }

        public void Added(params IToken[] items)
        {
            foreach (IToken item in items)
            {
                if (item is IPageElement)
                {
                    var pageElement = item as IPageElement;
                    pageElement!.Added(this);
                }
            }
        }

        public bool Empty()
        {
            return 0 == _tokens.Contents.Count;
        }

        public bool Exists(string match)
        {
            return Templates.Exists(ind => Regex.IsMatch(ind.Name, match, RegexOptions.IgnoreCase));
        }

        public Template? Find(string match)
        {
            return Templates.Find(ind => Regex.IsMatch(ind.Name, match, RegexOptions.IgnoreCase));
        }

        public Template? FindLast(string match)
        {
            return Templates.FindLast(ind => Regex.IsMatch(ind.Name, match, RegexOptions.IgnoreCase));
        }

        public List<Template> FindAll(string match)
        {
            return Templates.FindAll(ind => Regex.IsMatch(ind.Name, match, RegexOptions.IgnoreCase));
        }

        public Comment? FindComment(string match)
        {
            return _tokens.Contents.Find(ind => (ind is Comment comment && Regex.IsMatch(comment.Value, match, RegexOptions.IgnoreCase))) as Comment;
        }

        public bool ExistsNs(string ns)
        {
            return _tokens.Contents.Exists(ind => (ind.Type == TokenType.Link && ns.Equals(((Link)ind).Namespace.Text)));
        }

        public List<Link> FindAllNs(string ns)
        {
            var tokenList = _tokens.Contents.FindAll(x => (x.Type == TokenType.Link && ns.Equals(((Link)x).Namespace.Text)));
            return tokenList.ConvertAll(x => (Link)x);
        }

        public Section? FindSection(string match)
        {
            return Sections.Find(ind => (Regex.IsMatch(ind.Title, match, RegexOptions.IgnoreCase)));
        }

        public List<Section> FindAllSections(string match)
        {
            return Sections.FindAll(ind => (Regex.IsMatch(ind.Title, match, RegexOptions.IgnoreCase)));
        }

        public IToken? FindText(string match)
        {
            return _tokens.Contents.Find(ind => (ind.Type == TokenType.Text && Regex.IsMatch(ind.Text, match, RegexOptions.IgnoreCase)));
        }

        public List<IToken> FindAllText(string match)
        {
            return _tokens.Contents.FindAll(ind => (ind.Type == TokenType.Text && Regex.IsMatch(ind.Text, match, RegexOptions.IgnoreCase)));
        }

        public int FindIndex(IToken token)
        {
            return _tokens.Contents.FindIndex(x => x == token);
        }

        public int FindLastIndex()
        {
            return _tokens.Contents.Count - 1;
        }

        public Template Fetch(String name)
        {
            var t = Find(name);
            if (null == t)
            { 
                throw new Exception("Could not find " + name + " template");
            }
            return t;
        }

        public List<IToken> GetRange(int index, int count)
        {
            return _tokens.Contents.GetRange(index, count);
        }

        public IToken GetToken(int index)
        {
            return _tokens.Contents.ElementAt(index);
        }

        public void Insert(IToken token, string s)
        {
            _tokens.Contents.Insert(FindIndex(token), new Token(s));
        }

        public void Insert(IToken token, params IToken[] items)
        {
            int pos = FindIndex(token);
            _tokens.Contents.InsertRange(pos, items);
            Added(items);
        }

        public void Remove(Template template)
        {
            int pos = _tokens.Contents.FindIndex(x => x == template);
            if (pos >= 0)
            {
                _tokens.Contents.Remove(template);
                if (pos < _tokens.Contents.Count)
                {
                    var p = _tokens.Contents[pos];
                    if (String.IsNullOrWhiteSpace(p.Text))
                    {
                        _tokens.Contents.Remove(p);
                    }
                }
                Removed(template);
            }
        }

        public void RemoveRange(int index, int count)
        {
            List<IToken> items = _tokens.Contents.GetRange(index, count);
            _tokens.Contents.RemoveRange(index, count);
            Removed(items.ToArray());
        }

        public void Removed(params IToken[] items)
        {
            foreach (IToken item in items)
            {
                if (item is IPageElement)
                {
                    var pageElement = item as IPageElement;
                    pageElement!.Removed(this);
                }
            }
        }

        public void Replace(Template template1, Template template2)
        {
            Insert(template1, template2);
            Removed(template1);
        }

        public void Replace(string pattern, string replacement)
        {
            Regex regex = new Regex(pattern, RegexOptions.Multiline);
            Content = regex.Replace(Content, replacement);
        }

        public IToken Top()
        {
            return _tokens.Contents[0];
        }

        public IToken EndOfLine(IToken token)
        {
            var index = FindIndex(token);
            while (_tokens.Contents[index].Type != TokenType.Newline)
            {
                ++index;
            }
            return _tokens.Contents[index];
        }

        public IToken StartOfLine(IToken token)
        {
            var index = FindIndex(token);
            while (_tokens.Contents[index].Type != TokenType.Newline)
            {
                --index;
            }
            return _tokens.Contents[index + 1];
        }

        public List<IToken> GetLine(IToken token)
        {
            var result = new List<IToken>();
            var index = FindIndex(token);
            while (_tokens.Contents[index].Type != TokenType.Newline)
            {
                --index;
            }
            ++index;
            while (_tokens.Contents[index].Type != TokenType.Newline)
            {
                result.Add(_tokens.Contents[index]);
                 ++index;
           }
            return result;
        }

        public bool InCategory(string match)
        {
            return Categories().Exists(ind => Regex.IsMatch(ind, match, RegexOptions.IgnoreCase));
        }

        public List<string> Categories()
        {
            if (null == categories)
            {
                categories = Bot.Categories(Title);
            }
            return categories;
        }

        public void LoadFromStrings(string content)
        {
            Content = content;
        }

        public void LoadFromFile(string path)
        {
            Content = File.ReadAllText(path, Encoding.UTF8);
        }

        public void SaveToFile(string path)
        {
            File.WriteAllText(path, Content);
        }

        public void Load()
        {
            if (!loaded)
            {
                Bot.Load(this, RevId);
                loaded = true;
            }
        }

        public void Save(string summary)
        {
            Bot.Save(this, summary);
        }

        public void Save(Section section, string summary)
        {
            Bot.Save(section, summary);
        }

        public string Prediction()
        {
            if (null == prediction)
            {
                prediction = Bot.Prediction(Title);
            }
            return prediction;
        }

        public List<Revision> History()
        {
            if (null == Query)
            {
                Query = new Query(Title);
            }
            return Bot.History(Query);
        }

        public List<Revision> History(int limit)
        {
            if (null == Query)
            {
                Query = new Query(Title, limit);
            }
            return Bot.History(Query);
        }

        public List<Page> Redirects()
        {
            var query = new Query(Title);
            var redirects = new List<Page>();
            do
            {
                List<Page> batch = Bot.Redirects(query);
                redirects.AddRange(batch);
            } while (query.Continue);
            return redirects;
        }

        public int BytesCount()
        {
            int count = 0;
            foreach (var token in _tokens.Contents)
            {
                switch (token.Type)
                {
                    case TokenType.Comment:
                    case TokenType.ExternalLink:
                    case TokenType.Reference:
                    case TokenType.Section:
                    case TokenType.Table:
                    case TokenType.Template:
                        break;
                    case TokenType.Link:
                        Link link = (Link)token;
                        if (link.Namespace.Text.Equals(""))
                        {
                            count += link.Value.Length;
                        }
                        break;
                    default:
                        if (String.IsNullOrWhiteSpace(token.Text))
                            break;
                        count += token.Text.Length;
                        break;
                }
            }
            return count;
        }

        public int WordsCount()
        {
            int count = 0;
            foreach (var token in _tokens.Contents)
            {
                switch (token.Type)
                {
                    case TokenType.Comment:
                    case TokenType.ExternalLink:
                    case TokenType.Reference:
                    case TokenType.Section:
                    case TokenType.Table:
                    case TokenType.Template:
                        break;
                    case TokenType.Link:
                        Link link = (Link)token;
                        if (link.Namespace.Text.Equals(""))
                        {
                            count += link.Value.WordsCount();
                        }
                        break;
                    default:
                        count += token.Text.WordsCount();
                        break;
                }
            }
            return count;
        }

        public Page(string title)
        {
            Bot = new Bot();
            Title = title;
           _tokens = new Tokens();
            Links = new List<Link>();
            MilHist = new MilHist.MilHist(this);
            References = new List<Reference>();
            Sections = new List<Section>();
            Templates = new List<Template>();
            ArticleHistory = new ArticleHistory(this);
         }

        public Page(Bot bot, string title, int revid = -1) : this(title)
        {
            Bot = bot;
            RevId = revid;
        }

    }
}
