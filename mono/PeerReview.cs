﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wikimedia
{
	public class PeerReviewNominationsPage : NominationsPage
	{
		private readonly PeerReview nomination;
		private readonly List<NominationPage> nominations;

		public new List<NominationPage> Nominations
		{
			get
			{
				if (!nominations.Any())
				{
					Load();
					List<Template>? nominationTemplates = FindAll("CF/History peer reviews");
					foreach (Template template in nominationTemplates)
					{
						string name = "Wikipedia:" + template["1"];
						if (!string.IsNullOrEmpty(name))
						{
							NominationPage nominationPage = new NominationPage(nomination.Bot, nomination, template, name);
							nominations.Add(nominationPage);
						}
					}
				}
				return nominations;
			}
		}

		public PeerReviewNominationsPage(PeerReview nomination) : base(nomination)
		{
			this.nomination = nomination;
			nominations = new List<NominationPage>();
		}
	}

	public class PeerReview: Nomination
    {

        public new PeerReviewNominationsPage NominationsPage
        {
            get
            {
                return new PeerReviewNominationsPage(this);
            }
        }

        public PeerReview(Bot bot) : base(bot)
        {
            Action = "PR";
            Path = "User:AnomieBOT/C/History peer reviews";
            Regex = @"Wikipedia:Peer review/(?<title>.+)/archive\d+";
        }
    }
}
