using System;

public static class DateHelpers
{
    public static DateTime PreviousMonthFirstDay (this DateTime currentDate)
    {
        DateTime d = currentDate.PreviousMonthLastDay();
        return new DateTime  (d.Year, d.Month, 1, 0, 0, 0);
    }

    public static DateTime PreviousMonthLastDay (this DateTime currentDate)
    {
        return new DateTime (currentDate.Year, currentDate.Month, 1, 23, 59, 59).AddDays (-1);
    }

    public static string Month (this DateTime currentDate)
    {
        return currentDate.ToString ("MMMM");
    }

    public static string Timestamp (this DateTime currentDate)
    {
        return currentDate.ToString ("yyyyMMddHHmmss");
    }
}
