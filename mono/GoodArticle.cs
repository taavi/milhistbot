﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Wikimedia
{
	public class GANominationPage : NominationPage
	{
		private readonly Nomination Nomination;
		public new readonly Template Template;
		private ArticlePage? article;

		public new ArticlePage Article
		{
			get
			{
				if (null == article)
				{
					string? t = Template["1"];
					if (null == t)
                    {
						throw new ArgumentException("cannot find GA template argument 1");
                    }
                    article = new ArticlePage(Nomination, t);
				}
				return article;
			}
		}

		public GANominationPage(Bot bot, Nomination nomination, Template template, string name) : base(bot, nomination, template, name)
		{
			Template = template;
			Nomination = nomination;
		}
	}

	public class GANominationsPage : NominationsPage
	{
		private readonly GANomination GANomination;
		private readonly List<GANominationPage> nominations;

		public List<GANominationPage> GANominations
		{
			get
			{
				if (!nominations.Any())
				{
					Load();
					List<Template>? nominationTemplates = FindAll(GANomination.Regex);
					foreach (Template template in nominationTemplates)
					{
						string name = "Talk:" + template["1"] + "/GA" + template["2"];
						GANominationPage nominationPage = new GANominationPage(Bot, GANomination, template, name);
						nominations.Add(nominationPage);
					}
				}
				return nominations;
			}
		}
		public GANominationsPage(GANomination nomination) : base(nomination)
		{
			GANomination = nomination;
			nominations = new List<GANominationPage>();
		}
	}

	public class GANomination : Nomination
	{
		public GANominationsPage GANominationsPage
		{
			get
			{
				return new GANominationsPage(this);
			}
		}

		public GANomination(Bot bot) : base(bot)
		{
			Action = "GAN";
			Path   = "Wikipedia:Good article nominations";
			Regex  = "GANentry";
		}
	}

	public class GAReassessmentsPage : NominationsPage
	{
		private readonly GAReassessment GAReassessment;
		private readonly List<NominationPage> nominations;

		public List<NominationPage> GAReassessments
		{
			get
			{
				if (!nominations.Any())
				{
					Load();
					List<Template>? nominationTemplates = FindAll("CF/Wikipedia good article reassessment");
					foreach (Template template in nominationTemplates)
					{
						NominationPage nominationPage = new NominationPage(Bot, GAReassessment, template, "Wikipedia:" + template["1"]!);
						nominations.Add(nominationPage);
					}
				}
				return nominations;
			}
		}
		public GAReassessmentsPage(GAReassessment nomination) : base(nomination)
		{
			GAReassessment = nomination;
			nominations = new List<NominationPage>();
		}
	}

	public class GAReassessment : Nomination
	{
		public GAReassessmentsPage GAReassessmentsPage
		{
			get
			{
				return new GAReassessmentsPage(this);
			}
		}

		public GAReassessment(Bot bot) : base(bot)
		{
			Action = "GAR";
			Path   = "User:AnomieBOT/C/Wikipedia good article reassessment";
			Regex  = @"Wikipedia:Good article reassessment/(?<title>.+)/\d+";
		}
	}
}

